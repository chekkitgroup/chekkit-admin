export const config = {
  MAX_ATTACHMENT_SIZE: 5000000,
  s3: {
    BUCKET: "YOUR_S3_UPLOADS_BUCKET_NAME",
  },
  apiGateway: {
    REGION: "YOUR_API_GATEWAY_REGION",

    BASE_URL_AUTH:
       "http://ec2-34-212-121-47.us-west-2.compute.amazonaws.com:3000/api/v2",

    // BASE_URL_AUTH: "https://ci-api-staging.chekkit.app/api/v2",

    BASE_URL:
       "http://ec2-34-212-121-47.us-west-2.compute.amazonaws.com:3000/api/v2/admin",

    // BASE_URL: "https://ci-api-staging.chekkit.app/api/v2/admin",

  },
  cognito: {
    REGION: "YOUR_COGNITO_REGION",
    USER_POOL_ID: "YOUR_COGNITO_USER_POOL_ID",
    APP_CLIENT_ID: "YOUR_COGNITO_APP_CLIENT_ID",
    IDENTITY_POOL_ID: "YOUR_IDENTITY_POOL_ID",
  },
};
