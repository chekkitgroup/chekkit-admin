import React from "react";

const Toaster = React.lazy(() =>
  import("./views/notifications/toaster/Toaster")
);
const Tables = React.lazy(() => import("./views/base/tables/Tables"));

const Breadcrumbs = React.lazy(() =>
  import("./views/base/breadcrumbs/Breadcrumbs")
);
const Cards = React.lazy(() => import("./views/base/cards/Cards"));
const Carousels = React.lazy(() => import("./views/base/carousels/Carousels"));
const Collapses = React.lazy(() => import("./views/base/collapses/Collapses"));
const BasicForms = React.lazy(() => import("./views/base/forms/BasicForms"));

const Jumbotrons = React.lazy(() =>
  import("./views/base/jumbotrons/Jumbotrons")
);
const ListGroups = React.lazy(() =>
  import("./views/base/list-groups/ListGroups")
);
const Navbars = React.lazy(() => import("./views/base/navbars/Navbars"));
const Navs = React.lazy(() => import("./views/base/navs/Navs"));
const Paginations = React.lazy(() =>
  import("./views/base/paginations/Pagnations")
);
const Popovers = React.lazy(() => import("./views/base/popovers/Popovers"));
const ProgressBar = React.lazy(() =>
  import("./views/base/progress-bar/ProgressBar")
);
const Switches = React.lazy(() => import("./views/base/switches/Switches"));

const Tabs = React.lazy(() => import("./views/base/tabs/Tabs"));
const Tooltips = React.lazy(() => import("./views/base/tooltips/Tooltips"));
const BrandButtons = React.lazy(() =>
  import("./views/buttons/brand-buttons/BrandButtons")
);
const ButtonDropdowns = React.lazy(() =>
  import("./views/buttons/button-dropdowns/ButtonDropdowns")
);
const ButtonGroups = React.lazy(() =>
  import("./views/buttons/button-groups/ButtonGroups")
);
const Buttons = React.lazy(() => import("./views/buttons/buttons/Buttons"));
const Charts = React.lazy(() => import("./views/charts/Charts"));
const Dashboard = React.lazy(() => import("./views/dashboard/Dashboard"));
const CoreUIIcons = React.lazy(() =>
  import("./views/icons/coreui-icons/CoreUIIcons")
);
const Flags = React.lazy(() => import("./views/icons/flags/Flags"));
const Brands = React.lazy(() => import("./views/icons/brands/Brands"));
const Alerts = React.lazy(() => import("./views/notifications/alerts/Alerts"));
const Badges = React.lazy(() => import("./views/notifications/badges/Badges"));
const Modals = React.lazy(() => import("./views/notifications/modals/Modals"));
const Colors = React.lazy(() => import("./views/theme/colors/Colors"));
const Typography = React.lazy(() =>
  import("./views/theme/typography/Typography")
);
const Widgets = React.lazy(() => import("./views/widgets/Widgets"));
const Users = React.lazy(() => import("./views/users/Users"));
const Reports = React.lazy(() => import("./views/reports/Reports"));
const User = React.lazy(() => import("./views/users/User"));
const Reassign = React.lazy(() => import("./views/pins/Reassign"));
const Checkpins = React.lazy(() => import("./views/pins/Checkpins"));
const PinCheck = React.lazy(() => import("./views/pins/PinCheck"));
const AddProducts = React.lazy(() => import("./views/products/AddProducts"));
const ViewProducts = React.lazy(() => import("./views/products/ViewProducts"));
const AllBrands = React.lazy(() => import("./views/brands/AllBrands"));
const BrandProducts = React.lazy(() => import("./views/brands/BrandProducts"));
const SendMessage = React.lazy(() => import("./views/brands/SendMessage"));
const Settings = React.lazy(() => import("./views/settings/Settings"));
const CreateSurvey = React.lazy(() => import("./views/survey/CreateSurvey"));
const ViewSurveys = React.lazy(() => import("./views/survey/ViewSurveys.js"));
const CreateSurveyReward = React.lazy(() =>
  import("./views/survey/CreateSurveyReward")
);
const AddBatch = React.lazy(() => import("./views/products/AddSubProduct"));
const ViewBatches = React.lazy(() =>
  import("./views/products/ViewSubProducts")
);
const GenerateReport = React.lazy(() => import("./views/reports/generate-report"));
const Agents = React.lazy(() => import("./views/agent/agents"));
const CreateEditAgent = React.lazy(() => import("./views/agent/create-edit-agent"));

const user = JSON.parse(localStorage.getItem('chekkitSu'))?.user

let routes = [
  { path: "/", exact: true, name: "Home" },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  { path: "/theme", name: "Theme", component: Colors, exact: true },
  { path: "/theme/colors", name: "Colors", component: Colors },
  { path: "/theme/typography", name: "Typography", component: Typography },
  { path: "/base", name: "Base", component: Cards, exact: true },
  { path: "/base/breadcrumbs", name: "Breadcrumbs", component: Breadcrumbs },
  { path: "/base/cards", name: "Cards", component: Cards },
  { path: "/base/carousels", name: "Carousel", component: Carousels },
  { path: "/base/collapses", name: "Collapse", component: Collapses },
  { path: "/base/forms", name: "Forms", component: BasicForms },
  { path: "/base/jumbotrons", name: "Jumbotrons", component: Jumbotrons },
  { path: "/base/list-groups", name: "List Groups", component: ListGroups },
  { path: "/base/navbars", name: "Navbars", component: Navbars },
  { path: "/base/navs", name: "Navs", component: Navs },
  { path: "/base/paginations", name: "Paginations", component: Paginations },
  { path: "/base/popovers", name: "Popovers", component: Popovers },
  { path: "/base/progress-bar", name: "Progress Bar", component: ProgressBar },
  { path: "/base/switches", name: "Switches", component: Switches },
  { path: "/base/tables", name: "Tables", component: Tables },
  { path: "/base/tabs", name: "Tabs", component: Tabs },
  { path: "/base/tooltips", name: "Tooltips", component: Tooltips },
  { path: "/buttons", name: "Buttons", component: Buttons, exact: true },
  { path: "/buttons/buttons", name: "Buttons", component: Buttons },
  {
    path: "/buttons/button-dropdowns",
    name: "Dropdowns",
    component: ButtonDropdowns,
  },
  {
    path: "/buttons/button-groups",
    name: "Button Groups",
    component: ButtonGroups,
  },
  {
    path: "/buttons/brand-buttons",
    name: "Brand Buttons",
    component: BrandButtons,
  },
  { path: "/charts", name: "Charts", component: Charts },
  { path: "/icons", exact: true, name: "Icons", component: CoreUIIcons },
  { path: "/icons/coreui-icons", name: "CoreUI Icons", component: CoreUIIcons },
  { path: "/icons/flags", name: "Flags", component: Flags },
  { path: "/icons/brands", name: "Brands", component: Brands },
  {
    path: "/notifications",
    name: "Notifications",
    component: Alerts,
    exact: true,
  },
  { path: "/notifications/alerts", name: "Alerts", component: Alerts },
  { path: "/notifications/badges", name: "Badges", component: Badges },
  { path: "/notifications/modals", name: "Modals", component: Modals },
  { path: "/notifications/toaster", name: "Toaster", component: Toaster },
  { path: "/widgets", name: "Widgets", component: Widgets },
  { path: "/reports", exact: true, name: "Reports", component: Reports },
  { path: "/users", exact: true, name: "Users", component: Users },
  {
    path: "/product-pins/reassign",
    exact: true,
    name: "Reassign",
    component: Reassign,
  },
  {
    path: "/product-pins/check",
    exact: true,
    name: "Get Pin Status & Validity",
    component: Checkpins,
  },
  {
    path: "/product-pins/pin-check",
    exact: true,
    name: "Get Pin By SN or Sequence",
    component: PinCheck,
  },
  { path: "/users/:id", exact: true, name: "User Details", component: User },
  {
    path: "/products/add",
    exact: true,
    name: "Add Products",
    component: AddProducts,
  },
  {
    path: "/products/view-all",
    exact: true,
    name: "Add Products",
    component: ViewProducts,
  },
  {
    path: "/products/add-batch",
    exact: true,
    name: "Add Batch",
    component: AddBatch,
  },
  {
    path: "/products/view-batches",
    exact: true,
    name: "View Batches",
    component: ViewBatches,
  },
  { path: "/brands", exact: true, name: "All Brands", component: AllBrands },
  {
    path: "/brands/:brand_id/:brand_name/products",
    exact: true,
    name: "Brand Products",
    component: BrandProducts,
  },
  {
    path: "/brands/:brand_id/:brand_name/send-message",
    exact: true,
    name: "Send Message To Brand Authenticators",
    component: SendMessage,
  },
  {
    path: "/brands/:brand_id/:brand_name/products/:product_id/:product_name/send-message",
    exact: true,
    name: "Send Message To Product Authenticators",
    component: SendMessage,
  },
  {
    path: "/settings",
    exact: true,
    name: "Settings",
    component: Settings,
  },
  {
    path: "/create-survey",
    exact: true,
    name: "Create Survey",
    component: CreateSurvey,
  },
  {
    path: "/view-surveys",
    exact: true,
    name: "View Surveys",
    component: ViewSurveys,
  },
  {
    path: "/create-survey-reward",
    exact: true,
    name: "Create Survey Reward",
    component: CreateSurveyReward,
  },
  {
    path: "/generate-report",
    exact: true,
    name: "Genrate Report",
    component: GenerateReport,
  },
  {
    path: "/agents",
    exact: true,
    name: "Agents",
    component: Agents,
  },
  {
    path: "/agents/create-agent",
    exact: true,
    name: "Create Agent",
    component: CreateEditAgent,
  },
  {
    path: "/agents/edit-agent/:id",
    exact: true,
    name: "Edit Agent",
    component: CreateEditAgent,
  },
];

if (user.membership_type !== 'admin') {
  routes = routes.filter(a => a.name == 'Home' ||  a.name == 'Dashboard' || (a.path.includes('product-pins') && !a.path.includes('reassign')))
}

export default routes;
