import React, { useState, useEffect } from "react";
import FadeIn from "react-fade-in/lib/FadeIn";

import { CFormGroup, CLabel, CSelect } from "@coreui/react";

import Button from "../button";
import InputBox from "../input-box";
import QuestionList from "../question-list";
import SelectBox from "../select-box";

import RestService from "../../_services/rest.service";

const surveyData = {
  survey: { content: "", title: "", type: "" },
  question: [{ content: "", choices: [{ text: "" }, { text: "" }] }],
};

const SurveyEdit = ({ data, cb }) => {
  const [survey, setSurvey] = useState(surveyData);
  const [users, setUsers] = useState([]);
  const [userId, setUserId] = useState();
  const [isMetaSet, setMeta] = useState(false);
  const [message, setMessage] = useState("");

  useEffect(() => {
    if (data) {
      setSurvey(data);
      setUserId(data.userId);
    }
  }, [data]);

  useEffect(() => {
    RestService.getAllUsers().then(
      (res) => {
        setUsers(res.data);
      },
      (error) => {
        console.log(error);
      }
    );
  }, []);

  useEffect(() => {
    if (userId) {
      setSurvey({
        question: survey.question,
        survey: { ...survey.survey, user_id: userId },
      });
    }
  }, [userId]);

  const createSurvey = () => {
    if (!isMetaSet) {
      if (
        survey.survey.user_id &&
        survey.survey.title &&
        survey.survey.content &&
        survey.survey.type
      ) {
        setMeta(true);
      } else {
        if (
          !survey.survey.type &&
          survey.survey.title &&
          survey.survey.content
        ) {
        } else {
          alert("Enter survey details");
        }
      }
    } else {
      let questionComplete = survey.question.every(
        (question) => question.content
      );

      let answers = survey.question.map((question) => {
        if (typeof question.choices === 'string') {
          return JSON.parse(question.choices).every((choice) => choice.text)
        } else {
          return question.choices.every((choice) => choice.text)
        }
      });

      console.log(answers)

      let answerComplete = answers.every((answer) => answer);

      if (!questionComplete || !answerComplete) {
        alert("Incomplete survey data");
        return;
      }

      let x = { ...survey };

      let d = x.question.map((q) => {
        q.choices = JSON.stringify(q.choices);
        return q;
      });

      let y = { survey: x.survey, question: d };

      console.log(y);

      if (!data) {
        RestService.createSurvey(y)
          .then((res) => {
            setMessage("Survey Created Successfully" + " !!");
            setTimeout(() => {
              setMessage("");
              setSurvey(surveyData);
              setMeta(false);
            }, 2000);
          })
          .catch((e) => {
            setMessage(e.message + " !!");
            setTimeout(() => {
              setMessage("");
            }, 2000);
          });
      }else {
        RestService.updateSurvey(y.survey.slug ,y)
          .then((res) => {
            setMessage("Survey Updated Successfully" + " !!");
            setTimeout(() => {
              setMeta(false);
              setSurvey(surveyData);
              setMessage("");
              cb('done');
            }, 2000);
          })
          .catch((e) => {
            setMessage(e.message + " !!");
            setTimeout(() => {
              setMessage("");
            }, 2000);
          });
      }
    }
  };

  const onUserIdChange = (e) => {
    setUserId(e.target.value);
  };

  const onSurveyChange = (e) => {
    let { name, value } = e.target;
    let v = survey.survey.type ? survey.survey.type : "";
    if (name === "type") {
      v = value === "No" ? "2" : "1";
      let sv = { ...survey.survey, [name]: v };
      setSurvey({ question: survey.question, survey: sv });
    } else {
      if (!v) v = "1";
      let sv = {
        ...survey.survey,
        [name]: value,
        type: survey.survey.type ? survey.survey.type : v,
      };
      setSurvey({ question: survey.question, survey: sv });
    }
  };

  return (
    <FadeIn className="w-full px-4 sm:px-6 md:px-8 flex flex-col justify-center">
      <div
        className={`flex flex-col justify-center items-center bg-white rounded-xl p-4 w-full space-y-6`}
      >
        {message && (
          <div className={`text-center font-semibold`}>{message}</div>
        )}
        {!isMetaSet ? (
          <FadeIn className={`w-full space-y-6`}>
            <CFormGroup>
              <CLabel htmlFor="ccaccount" className="m-2">
                Select Account
              </CLabel>
              <CSelect
                custom
                name="ccaccount"
                id="ccaccount"
                onChange={onUserIdChange}
                value={userId}
              >
                <option value="user_id">Select your option</option>
                {users.length > 0 &&
                  users.map((item) => (
                    <option key={item.id} value={item.id}>
                      {item.company_rep
                        ? item.company_rep+ " " + '('+ ' ' + item.username + ' ' +')'
                        : "" + " - " + item.first_name
                        ? item.first_name
                        : ""}
                    </option>
                  ))}
              </CSelect>
            </CFormGroup>
            <InputBox
              label={`Survey Title`}
              name={`title`}
              value={survey.survey.title}
              onValueChange={onSurveyChange}
              labelColor={`text-gray-500`}
              placeholder={`Type title here...`}
              variant={3}
            />
            <SelectBox
              className={`mb-6 mt-10`}
              label={`Does this survey have a Label ?`}
              options={["Yes", "No"]}
              variant={3}
              name={`type`}
              value={
                !survey.survey.type
                  ? `Yes`
                  : survey.survey.type === "1" || survey.survey.type === 1
                  ? "Yes"
                  : "No"
              }
              onValueChange={onSurveyChange}
            />
            <InputBox
              label={`Survey Intent / Content`}
              name={`content`}
              value={survey.survey.content}
              onValueChange={onSurveyChange}
              labelColor={`text-gray-500`}
              placeholder={`Type content here...`}
              variant={3}
            />
          </FadeIn>
        ) : (
          users.length > 0 && (
            <QuestionList survey={survey} setSurvey={setSurvey} />
          )
        )}
        <div className={`w-full flex justify-between items-end pt-6`}>
          <div>
            {(isMetaSet || data) && (
                <Button
                  text={`Back`}
                  type={`secondary`}
                  size={2}
                  onClick={() => {
                    setMeta(false);
                    if (data && !isMetaSet) cb();
                  }}
                />
              )}
          </div>
          <div
            className={`flex items-end lg:items-center flex-col space-y-6 lg:flex-row lg:space-y-0  lg:space-x-6`}
          >
            <Button
              text={`${
                !isMetaSet ? `Next` : data ? `Update Survey` : `Create Survey`
              }`}
              size={2}
              onClick={createSurvey}
            />
          </div>
        </div>
      </div>
    </FadeIn>
  );
};

export default SurveyEdit;
