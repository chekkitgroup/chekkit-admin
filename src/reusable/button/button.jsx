import React from "react";

const Button = ({
  text,
  type,
  variant,
  light,
  cx,
  size,
  className,
  icon,
  placement,
  onClick,
}) => {
  return (
    <>
      {!cx && (
        <button
          onClick={onClick}
          style={{ minWidth: "150px" }}
          className={`${className} ${
            variant !== 1
              ? type === "secondary"
                ? `bg-transparent text-blue-900 border-blue-900  hover:bg-blue-900`
                : light
                ? `bg-white text-blue-900 hover:bg-blue-900  hover:border-blue-900`
                : `bg-blue-900 text-white border-blue-900`
              : type === "secondary"
              ? `bg-transparent text-green-400 border-green-400 hover:bg-green-400`
              : `bg-green-400 border-green-400 text-blue-900`
          } transition_all border-2 border-transparent ${
            size && size === 2 ? `px-2 py-3 text-sm` : `px-6 py-4 text-lg`
          } rounded-lg flex justify-center items-center hover:shadow-lg`}
        >
          <div>
            {icon && placement === "left" && icon}
            <span className={`px-2 font-semibold`}>{text}</span>
            {icon && placement === "right" && icon}
          </div>
        </button>
      )}
      {cx === 2 && (
        <button
          onClick={onClick}
          className={`${className} ${
            variant !== 1
              ? type === "secondary"
                ? `bg-transparent text-blue-900 border-blue-900 hover:bg-blue-900`
                : light
                ? `bg-white text-blue-900 hover:bg-blue-900  hover:border-blue-900`
                : `bg-blue-900 text-white border-blue-900`
              : type === "secondary"
              ? `bg-transparent text-green-400 border-green-400 hover:bg-green-400`
              : `bg-green-400 border-green-400 text-blue-900`
          } transition_all border-2 border-transparent ${
            size && size === 2 ? `px-2 py-3 text-sm` : `px-6 py-4 text-lg`
          } rounded-lg flex justify-center items-center hover:shadow-lg`}
        >
          <div>
            {icon && placement === "left" && icon}
            <span className={`px-2 font-semibold`}>{text}</span>
            {icon && placement === "right" && icon}
          </div>
        </button>
      )}
    </>
  );
};

export default Button;
