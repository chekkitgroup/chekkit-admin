import axios from "axios";
import { config } from "../config";

axios.defaults.headers.common['source'] = 'web';

class RestService {
  getAllUsers() {
    return this.DoGet("/list-users");
  }

  getUserProducts(id) {
    return this.DoGet(`/get-user-products?id=${id}`);
  }

  // Rewards
  createSurveyReward(data) {
    return this.DoPost(`/rewards`, data);
  }

  getUserRewards(id) {
    return this.DoGet(`/user-survey-rewards/${id}`);
  }

  // Agents
  createAgent(data) {
    return this.DoPost(`/agents`, data);
  }

  getAgents() {
    return this.DoGet(`/agents`);
  }

  getAgent(agent_id) {
    return this.DoGet(`/agents/${agent_id}`);
  }

  updateAgent(agent_id, data) {
    return this.DoPut(`/agents/${agent_id}`, data);
  }

  deleteAgent(agent_id) {
    return this.DoDelete(`/agents/${agent_id}`);
  }

  // Report Generation
  genrateReport(from, to, subProductId) {
    return this.DoGet(
      `/scan-history/report/${subProductId}?from=${from}&to=${to}`
    );
  }

  // Survey
  createSurvey(data) {
    return this.DoPost(`/surveys`, data);
  }

  getSurvey(slug) {
    return this.DoGet(`/surveys/${slug}`);
  }

  getSurveyQuestions(surveyId) {
    return this.DoGet(`/survey-questions/${surveyId}`);
  }

  updateSurvey(slug, data) {
    return this.DoPut(`/surveys/${slug}`, data);
  }

  getUserSurveys(id) {
    return this.DoGet(`/get-user-surveys/${id}`);
  }

  createBatch(data) {
    return this.DoPost(`/sub-products`, data);
  }

  getBatches(id) {
    return this.DoGet(`/sub-products/${id}`);
  }

  getProductBatches(id) {
    return this.DoGet(`/get-product-batches?id=${id}`);
  }

  updateBatch(batch_num, data) {
    return this.DoPut(`/sub-products/${batch_num}`, data);
  }

  getProductDetails(id) {
    return this.DoGet(`/products/${id}`);
  }

  getProducts(id, per_page, page) {
    return this.DoGet(
      `/products?page=${page}&per_page=${per_page}&user_id=${id}`
    );
  }

  getBrands(per_page, page) {
    return this.DoGet(`/brands?per_page=${per_page}&page=${page}`);
  }

  // Settings
  getSettings() {
    return this.DoGet(`/settings`);
  }

  updateSettings(id, data) {
    return this.DoPut(`/settings/${id}`, data);
  }

  getBrandProducts(id, per_page, page) {
    return this.DoGet(
      `/brands/${id}/products?per_page=${per_page}&page=${page}`
    );
  }

  sendSMSProductAuthenticators(id, data) {
    return this.DoPost(`/products/${id}/sms`, data);
  }

  sendSMSBrandAuthenticators(id, data) {
    return this.DoPost(`/brands/${id}/sms`, data);
  }

  getProductBatches(id) {
    return this.DoGet(`/get-product-batches?id=${id}`);
  }

  getDashboardStats() {
    return this.DoGet("/get-dashboard-statistics");
  }

  getProductReports() {
    return this.DoGet("/get-product-reports");
  }

  addProducts(data) {
    return this.DoPost("/products", data);
  }

  updateProducts(data) {
    return this.DoPut("/products/soda-and-soft-drink-776463aaae19", data);
  }

  reassignPins(data) {
    return this.DoPost("/reassign-pins", data);
  }

  checkPin(data) {
    return this.DoPost("/check-pin", data);
  }

  updatePin(data) {
    return this.DoPost("/update-pin-status", data);
  }

  getPinsBySequence(data, query = "") {
    return this.DoGet(`/pins/${data.sequence_value}?${query}`);
  }

  getPinBySerialNumber(data) {
    return this.DoGet(`/pin/${data.serial_value}`);
  }

  async login(username, password) {
    const response = await axios.post(
      config.apiGateway.BASE_URL_AUTH + "/auth/signin",
      {
        username,
        password,
      }
    );
    console.log(response.data.data);
    if (response.data.data.token) {
      localStorage.setItem("chekkitSu", JSON.stringify(response.data.data));
    }
    return response.data;
  }

  logout() {
    localStorage.removeItem("chekkitSu");
  }

  register(username, email, password) {
    return axios.post(config.apiGateway.BASE_URL_AUTH + "/auth/signup", {
      username,
      email,
      password,
    });
  }

  isAuthenticated(token) {
    if (token) return true;
  }

  getCurrentUser() {
    let u = JSON.parse(localStorage.getItem("chekkitSu"));
    return u;
  }

  async DoGet(url) {
    let u = JSON.parse(localStorage.getItem("chekkitSu"));
    var token = "";
    if (this.isAuthenticated && u) {
      token = u.token;
    }
    console.log(u);
    const response = await axios.get(config.apiGateway.BASE_URL + url, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    return response.data;
  }

  async DoGet2(url) {
    let u = JSON.parse(localStorage.getItem("chekkitSu"));
    var token = "";
    if (this.isAuthenticated && u) {
      token = u.token;
    }
    console.log(u);
    const response = await axios.get(config.apiGateway.BASE_URL_AUTH + url, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    return response.data;
  }

  // DoGetStaging(url) {
  //   let u = JSON.parse(localStorage.getItem("chekkitSu"));
  //   var token = "";
  //   if (this.isAuthenticated && u) {
  //     token = u.token;
  //   }
  //   console.log(u);
  //   return axios
  //     .get(config.apiGateway.BASE_URL_STAGING + url, {
  //       headers: {
  //         Authorization: `Bearer ${token}`,
  //       },
  //     })
  //     .then((response) => {
  //       return response.data;
  //     });
  // }

  // DoGetStaging2(url) {
  //   let u = JSON.parse(localStorage.getItem("chekkitSu"));
  //   var token = "";
  //   if (this.isAuthenticated && u) {
  //     token = u.token;
  //   }
  //   console.log(u);
  //   return axios
  //     .get(config.apiGateway.BASE_URL_AUTH_STAGING + url, {
  //       headers: {
  //         Authorization: `Bearer ${token}`,
  //       },
  //     })
  //     .then((response) => {
  //       return response.data;
  //     });
  // }

  async DoPost(url, payload) {
    let u = JSON.parse(localStorage.getItem("chekkitSu"));
    var token = "";
    if (this.isAuthenticated && u) {
      token = u.token;
    }
    console.log(u);
    const response = await axios.post(
      config.apiGateway.BASE_URL + url,
      payload,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    return response.data;
  }

  async DoPost2(url, payload) {
    let u = JSON.parse(localStorage.getItem("chekkitSu"));
    var token = "";
    if (this.isAuthenticated && u) {
      token = u.token;
    }
    console.log(u);
    const response = await axios.post(
      config.apiGateway.BASE_URL_AUTH + url,
      payload,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    return response.data;
  }

  async DoPut(url, payload) {
    let u = JSON.parse(localStorage.getItem("chekkitSu"));
    var token = "";
    if (this.isAuthenticated && u) {
      token = u.token;
    }
    console.log(u);
    const response = await axios.put(
      config.apiGateway.BASE_URL + url,
      payload,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    return response.data;
  }

  async DoPut2(url, payload) {
    let u = JSON.parse(localStorage.getItem("chekkitSu"));
    var token = "";
    if (this.isAuthenticated && u) {
      token = u.token;
    }
    console.log(u);
    const response = await axios.put(
      config.apiGateway.BASE_URL_AUTH + url,
      payload,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    return response.data;
  }

  async DoDelete(url) {
    let u = JSON.parse(localStorage.getItem("chekkitSu"));
    var token = "";
    if (this.isAuthenticated && u) {
      token = u.token;
    }
    console.log(u);
    const response = await axios.delete(config.apiGateway.BASE_URL + url, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    return response.data;
  }

}

export default new RestService();
