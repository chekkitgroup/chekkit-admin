import axios from "axios";
import { config } from "../config";

axios.defaults.headers.common['source'] = 'web';

const API_URL = config.apiGateway.BASE_URL_AUTH + "/auth/";

class AuthService {
  login(username, password) {
    return axios
      .post(API_URL + "signin", {
        username,
        password,
      })
      .then((response) => {
        console.log(response.data.data);
        if (response.data.data.token) {
          localStorage.setItem("chekkitSu", JSON.stringify(response.data.data));
        }

        return response.data;
      });
  }

  logout() {
    console.log("logged out");
    localStorage.removeItem("chekkitSu");
  }

  register(username, email, password) {
    return axios.post(API_URL + "signup", {
      username,
      email,
      password,
    });
  }

  isAuthenticated(token) {
    if (token) return true;
  }

  getCurrentUser() {
    let u = JSON.parse(localStorage.getItem("chekkitSu"));
    return u;
  }
}

export default new AuthService();
