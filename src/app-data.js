export const brands = [
  {
    id: 1,
    name: 'Nike',
    desc: 'Some Brand desc',
    products: [
      {
        id: 11,
        name: 'Shoe',
        desc: 'Some Product desc',
      }
    ]
  },
  {
    id: 2,
    name: 'Gucci',
    desc: 'Some Brand desc',
    products: [
      {
        id: 22,
        name: 'Shirt',
        desc: 'Some Product desc',
      }
    ]
  },
  {
    id: 3,
    name: 'Givenchy',
    desc: 'Some Brand desc',
    products: [
      {
        id: 33,
        name: 'Bag',
        desc: 'Some Product desc',
      }
    ]
  },
  {
    id: 4,
    name: 'Adidas',
    desc: 'Some Brand desc',
    products: [
      {
        id: 44,
        name: 'Soccer',
        desc: 'Some Product desc',
      }
    ]
  },
]
