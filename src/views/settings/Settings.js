import React, { useState, useEffect } from "react";
import { HiPencil } from "react-icons/hi";

import {
  CModal,
  CModalBody,
  CModalHeader,
  CModalTitle,
  CCol,
  CInput,
  CInputGroup,
  CRow,
} from "@coreui/react";

import Form from "react-validation/build/form";

import RestService from "../../_services/rest.service";

const Settings = () => {
  const [state, setState] = useState({
    settings: undefined,
    modal: false,
    modal2: false,
    loading: true,
    message: "",
    messageType: "",
    updateValue: "",
  });

  const setModal = () => {
    setState({
      ...state,
      updateValue: "",
      message: "",
      modal: !state.modal,
    });
  };

  const setModal2 = () => {
    setState({
      ...state,
      updateValue: "",
      message: "",
      modal2: !state.modal2,
    });
  };

  const onUpdateValueChange = (e) => {
    setState({ ...state, updateValue: e.target.value });
  };

  const fetchSettings = () => {
    RestService.getSettings().then(
      (res) => {
        setState({
          ...state,
          settings: res.data,
          loading: false,
          message: "",
        });

        if (!res.status) {
          setState({ ...state, loading: false, message: res.message });
        }
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setState({ ...state, loading: false, message: resMessage });
      }
    );
  };

  useEffect(() => {
    if (state.modal) setModal();
    if (state.modal2) setModal2();
  }, [state.settings]);

  const updateData = (e, id) => {
    e.preventDefault();

    if (!state.updateValue) return;

    setState({ ...state, message: "", loading: true });

    let data = { value: state.updateValue };

    RestService.updateSettings(id, data).then(
      (res) => {
        setState({
          ...state,
          loading: false,
          message: res.message,
          messageType: "success",
        });

        fetchSettings();

        if (!res.status) {
          setState({ ...state, loading: false, message: res.message });
        }
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setState({ ...state, loading: false, message: resMessage });
      }
    );
  };

  useEffect(() => {
    RestService.getSettings().then(
      (res) => {
        setState({
          ...state,
          settings: res.data,
          loading: false,
          message: "",
        });

        if (!res.status) {
          setState({ ...state, loading: false, message: res.message });
        }
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setState({ ...state, loading: false, message: resMessage });
      }
    );
  }, []);

  return (
    <div className="c-app c-default-layout w-full">
      <CModal
        show={state.modal}
        // onClose={setModal}
      >
        <CModalHeader closeButton>
          <CModalTitle>Update Points Per Naira</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <Form onSubmit={(e) => updateData(e, 1)}>
            <CInputGroup className="mb-3">
              <CInput
                type="number"
                value={state.updateValue}
                onChange={onUpdateValueChange}
                placeholder="Points Per Naira"
                required
              />
            </CInputGroup>
            <CRow>
              <CCol xs="12">
                <button
                  className="btn btn-success btn-block"
                  disabled={state.loading}
                >
                  {state.loading && (
                    <span className="spinner-border spinner-border-sm"></span>
                  )}
                  <span>Update</span>
                </button>
              </CCol>
            </CRow>
            {state.message && (
              <div className={`form-group mt-2`}>
                <div
                  className={`${
                    state.messageType === "success"
                      ? `alert alert-success`
                      : `alert alert-danger`
                  }`}
                  role="alert"
                >
                  {state.message}
                </div>
              </div>
            )}
          </Form>
        </CModalBody>
      </CModal>

      <CModal
        show={state.modal2}
        // onClose={setModal2}
      >
        <CModalHeader closeButton>
          <CModalTitle>Update Points Per USD</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <Form onSubmit={(e) => updateData(e, 2)}>
            <CInputGroup className="mb-3">
              <CInput
                type="number"
                value={state.updateValue}
                onChange={onUpdateValueChange}
                placeholder="Points Per USD"
                required
              />
            </CInputGroup>
            <CRow>
              <CCol xs="12">
                <button
                  className="btn btn-success btn-block"
                  disabled={state.loading}
                >
                  {state.loading && (
                    <span className="spinner-border spinner-border-sm"></span>
                  )}
                  <span>Update</span>
                </button>
              </CCol>
            </CRow>
            {state.message && (
              <div className="form-group mt-2">
                <div
                  className={`${
                    state.messageType === "success"
                      ? `alert alert-success`
                      : `alert alert-danger`
                  }`}
                  role="alert"
                >
                  {state.message}
                </div>
              </div>
            )}
          </Form>
        </CModalBody>
      </CModal>

      <div
        className={`flex flex-col space-y-4 lg:flex-row lg:space-x-4 lg:space-y-0 w-full`}
      >
        {state.loading && !state.settings && (
          <div className="spinner-border spinner-border-sm mx-auto mt-6 w-10"></div>
        )}
        {state.settings &&
          state.settings.map((setting, idx) => {
            return (
              <>
                {setting.name === "points_per_naira" && (
                  <div
                    key={setting.id}
                    className={`bg-white p-4 flex justify-center items-center space-x-2 w-full h-20`}
                  >
                    <span className={`font-semibold text-gray-800`}>
                      Points Per Naira
                    </span>
                    <span className={`font-semibold text-gray-800`}>:</span>
                    <span className={`font-semibold text-blue-600`}>
                      {setting.value}
                    </span>
                    <div className={`flex-1 flex justify-end`}>
                      <div
                        className={`rounded-full p-2 text-gray-800 hover:bg-gray-100 cursor-pointer`}
                      >
                        <HiPencil onClick={() => setModal()} size={20} />
                      </div>
                    </div>
                  </div>
                )}
                {setting.name === "points_per_usd" && (
                  <div
                    key={setting.id}
                    className={`bg-white p-4 flex justify-center items-center space-x-2 w-full h-20`}
                  >
                    <span className={`font-semibold text-gray-800`}>
                      Points Per USD
                    </span>
                    <span className={`font-semibold text-gray-800`}>:</span>
                    <span className={`font-semibold text-blue-600`}>
                      {setting.value}
                    </span>
                    <div className={`flex-1 flex justify-end`}>
                      <div
                        className={`rounded-full p-2 text-gray-800 hover:bg-gray-100 cursor-pointer`}
                      >
                        <HiPencil onClick={() => setModal2()} size={20} />
                      </div>
                    </div>
                  </div>
                )}
              </>
            );
          })}
      </div>
    </div>
  );
};

export default Settings;
