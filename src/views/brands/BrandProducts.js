import React, { useState, useEffect } from "react";

import {
  CCardHeader,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CRow,
  CButton,
  CPagination,
} from "@coreui/react";

import { useHistory, useParams } from "react-router-dom";

import RestService from "../../_services/rest.service";

const BrandProducts = () => {
  const history = useHistory();
  const { brand_id, brand_name } = useParams();
  const [currentPage, setCurrentPage] = useState(1);
  const [numsPage, setNumsPage] = useState(1);
  const [state, setState] = useState({
    products: undefined,
    currentPage: "",
    loading: true,
    message: "",
    messageType: "",
  });

  const getNumsPages = (totalItems) => {
    return Math.ceil(parseFloat(totalItems) / 15);
  };

  useEffect(() => {
    if (brand_id) {
      RestService.getBrandProducts(brand_id, 15, currentPage).then(
        (res) => {
          // this.resetForm()
          setState({
            ...state,
            products: res.data.products,
            loading: false,
            message:
              res.data.products.length > 0 ? "" : "Product list is empty",
          });
          setNumsPage(getNumsPages(res.data.metadata.totalItems));

          if (!res.status) {
            setState({ ...state, loading: false, message: res.message });
          }
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          setState({ ...state, loading: false, message: resMessage });
        }
      );
    } // eslint-disable-next-line
  }, [currentPage, brand_id]);

  return (
    <div className="c-app c-default-layout">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="12" lg="12" xl="12">
            {brand_id && (
              <CCard className="mx-4">
                <CCardHeader className="p-4 flex items-center justify-between">
                  <div className={`capitalize`}>{`${brand_name} Products`}</div>
                  <CButton
                    onClick={() =>
                      history.push(
                        `/brands/${brand_id}/${brand_name}/send-message`.toLowerCase()
                      )
                    }
                    color="primary"
                  >
                    Send Message
                  </CButton>
                </CCardHeader>
                {state.message && (
                  <span className="mx-auto mt-6 bg-red-400 text-white px-4 py-2 rounded">
                    {state.message}
                  </span>
                )}
                {state.loading && (
                  <span className="spinner-border spinner-border-sm mx-auto mt-6"></span>
                )}
                <CCardBody className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-6">
                  {state.products &&
                    state.products.length > 0 &&
                    state.products.map((product) => {
                      return (
                        <CCard
                          onClick={() =>
                            history.push(
                              `/brands/${brand_id}/${brand_name}/products/${product.id}/${product.product_name}/send-message`.toLowerCase()
                            )
                          }
                          key={product.id}
                          className="p-4 cursor-pointer hover:shadow"
                        >
                          <div className={`flex gap-2`}>
                            <div className={`w-16 h-16`}>
                              <img
                                className={`w-full h-full rounded`}
                                src={product.product_image}
                                alt="product"
                              />
                            </div>
                            <div>
                              <div className="font-semibold">
                                {product.product_name}
                              </div>
                              <div className="text-sm">{product.product_description}</div>
                              <div className="text-sm">{product.producer_name}</div>
                            </div>
                          </div>
                        </CCard>
                      );
                    })}
                </CCardBody>
                <div className={`mx-auto`}>
                  {state.products && state.products.length > 0 && (
                    <CPagination
                      activePage={currentPage}
                      pages={numsPage}
                      onActivePageChange={(i) => setCurrentPage(i)}
                    ></CPagination>
                  )}
                </div>
              </CCard>
            )}
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default BrandProducts;
