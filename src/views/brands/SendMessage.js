import React, { useState } from "react";

import {
  CCardHeader,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CRow,
  CButton,
} from "@coreui/react";

import { useParams, useHistory } from "react-router-dom";

import RestService from "../../_services/rest.service";

const SendMessage = () => {
  const history = useHistory()
  const { brand_id, product_id, brand_name, product_name } = useParams();
  const [state, setState] = useState({
    text: "",
    loading: false,
    message: "",
    messageType: "",
  });

  const sendMessage = () => {
    if (state.text) {
      setState({
        ...state,
        loading: true,
      });

      if (brand_id && !product_id) {
        // Send message to brand authenticators
        RestService.sendSMSBrandAuthenticators(brand_id, {message: state.text}).then(
          (res) => {
            setState({
              ...state,
              text: "",
              loading: false,
              message: res.message,
              messageType: "success",
            });

            setTimeout(() => {
              setState({
                ...state,
                text: "",
                message: "",
                messageType: "",
              });
              history.push(`/brands/${brand_id}/${brand_name}/products`)
            }, 2000);

            if (!res.status) {
              setState({ ...state, loading: false, message: res.message });
            }
          },
          (error) => {
            const resMessage =
              (error.response &&
                error.response.data &&
                error.response.data.message) ||
              error.message ||
              error.toString();

            setState({ ...state, loading: false, message: resMessage });
            setTimeout(() => {
              setState({
                ...state,
                message: "",
              });
            }, 2000);
          }
        );
      }

      if (brand_id && product_id) {
        // Send message to product authenticators
        RestService.sendSMSProductAuthenticators( product_id, {message: state.text}).then(
          (res) => {
            setState({
              ...state,
              text: "",
              loading: false,
              message: res.message,
              messageType: "success",
            });

            setTimeout(() => {
              setState({
                ...state,
                text: "",
                message: "",
                messageType: "",
              });
              history.push(`/brands/${brand_id}/${brand_name}/products`)
            }, 2000);

            if (!res.status) {
              setState({ ...state, loading: false, message: res.message });
            }
          },
          (error) => {
            const resMessage =
              (error.response &&
                error.response.data &&
                error.response.data.message) ||
              error.message ||
              error.toString();

            setState({ ...state, loading: false, message: resMessage });
            setTimeout(() => {
              setState({
                ...state,
                message: "",
              });
            }, 2000);
          }
        );
      }
    } else {
      setState({
        ...state,
        message: "Enter Message...",
      });
      setTimeout(() => {
        setState({
          ...state,
          message: "",
        });
      }, 2000);
      return;
    } // eslint-disable-next-line
  };

  const handleInputChange = (event) => {
    const { value } = event.target;
    setState({
      ...state,
      text: value,
    });
  };

  return (
    <div className="c-app c-default-layout">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="12" lg="12" xl="12">
            {brand_id && !product_id && (
              <CCard className="mx-4">
                <CCardHeader className="p-4 flex items-center justify-between">
                  <div>
                    <span>{`Send Message to users that authenticated `}</span>
                    <span className={`font-semibold text-blue-600 capitalize`}>
                      {`${brand_name} Products`}
                    </span>
                  </div>
                </CCardHeader>
                {state.message && (
                  <span
                    className={`${
                      state.messageType === `success`
                        ? `bg-green-400`
                        : `bg-red-400`
                    } mx-auto mt-6 text-white px-4 py-2 rounded`}
                  >
                    {state.message}
                  </span>
                )}
                <CCardBody>
                  <form className={`flex flex-col w-full space-y-4`}>
                    <textarea
                      value={state.text}
                      onChange={handleInputChange}
                      placeholder={`message...`}
                      className={`w-full border border-gray-300 p-2 rounded-lg resize-none`}
                      rows={12}
                    />
                    <CButton
                      onClick={sendMessage}
                      className={`w-full flex items-center justify-center space-x-2`}
                      color="primary"
                      disabled={state.loading}
                    >
                      <span>
                        {state.loading && (
                          <span className="spinner-border spinner-border-sm"></span>
                        )}
                      </span>
                      <span>Send Message</span>
                    </CButton>
                  </form>
                </CCardBody>
              </CCard>
            )}
            {brand_id && product_id && (
              <CCard className="mx-4">
                <CCardHeader className="p-4 flex items-center justify-between">
                  <div>
                    <span>{`Send Message to users that authenticated `}</span>
                    <span className={`font-semibold text-blue-600 capitalize`}>
                      {product_name}
                    </span>
                  </div>
                </CCardHeader>
                {state.message && (
                  <span
                    className={`${
                      state.messageType === `success`
                        ? `bg-green-400`
                        : `bg-red-400`
                    } mx-auto mt-6 text-white px-4 py-2 rounded`}
                  >
                    {state.message}
                  </span>
                )}
                <CCardBody>
                  <form className={`flex flex-col w-full space-y-4`}>
                    <textarea
                      value={state.text}
                      onChange={handleInputChange}
                      placeholder={`message...`}
                      className={`w-full border border-gray-300 p-2 rounded-lg resize-none`}
                      rows={12}
                    />
                    <CButton
                      onClick={sendMessage}
                      className={`w-full flex items-center justify-center space-x-2`}
                      color="primary"
                      disabled={state.loading}
                    >
                      <span>
                        {state.loading && (
                          <span className="spinner-border spinner-border-sm"></span>
                        )}
                      </span>
                      <span>Send Message</span>
                    </CButton>
                  </form>
                </CCardBody>
              </CCard>
            )}
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default SendMessage;
