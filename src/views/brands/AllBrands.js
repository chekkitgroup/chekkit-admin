import React, { useState, useEffect } from "react";

import {
  CCardHeader,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CRow,
  CPagination,
} from "@coreui/react";

import { useHistory } from "react-router-dom";

import RestService from "../../_services/rest.service";

const AllBrands = () => {
  const history = useHistory();
  const [currentPage, setCurrentPage] = useState(1);
  const [numsPage, setNumsPage] = useState(1);
  const [state, setState] = useState({
    brands: undefined,
    currentPage: "",
    loading: true,
    message: "",
    messageType: "",
  });

  const getNumsPages = (totalItems) => {
    return Math.ceil(parseFloat(totalItems) / 15);
  };

  useEffect(() => {
    RestService.getBrands(15, currentPage).then(
      (res) => {
        // this.resetForm()
        setState({
          ...state,
          brands: res.data.brands,
          loading: false,
          message: res.data.brands.length > 0 ? "" : "Brand list is empty.",
        });
        setNumsPage(getNumsPages(res.data.metadata.totalItems));

        if (!res.status) {
          setState({ ...state, loading: false, message: res.message });
        }
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setState({ ...state, loading: false, message: resMessage });
      }
    ); // eslint-disable-next-line
  }, [currentPage]);

  return (
    <div className="c-app c-default-layout">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="12" lg="12" xl="12">
            <CCard className="mx-4">
              <CCardHeader className="text-center p-4">All Brands</CCardHeader>
              {state.message && (
                <span className="mx-auto mt-6 bg-red-400 text-white px-4 py-2 rounded">
                  {state.message}
                </span>
              )}
              {state.loading && (
                <span className="spinner-border spinner-border-sm mx-auto mt-6"></span>
              )}
              <CCardBody className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-6">
                {state.brands &&
                  state.brands.length > 0 &&
                  state.brands.map((brand) => {
                    return (
                      <CCard
                        onClick={() =>
                          history.push(
                            `/brands/${brand.id}/${brand.username}/products`.toLowerCase()
                          )
                        }
                        key={brand.id}
                        className="p-4 cursor-pointer hover:shadow"
                      >
                        <div className="font-semibold">{brand.username}</div>
                        <div className="">{brand.company_rep}</div>
                      </CCard>
                    );
                  })}
              </CCardBody>
              <div className={`mx-auto`}>
                {state.brands && state.brands.length > 0 && (
                  <CPagination
                    activePage={currentPage}
                    pages={numsPage}
                    onActivePageChange={(i) => setCurrentPage(i)}
                  ></CPagination>
                )}
              </div>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default AllBrands;
