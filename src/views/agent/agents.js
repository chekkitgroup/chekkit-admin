import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

import {
  CCard,
  CCol,
  CRow,
} from "@coreui/react";
import RestService from "../../_services/rest.service";

const Agents = () => {
  const history = useHistory()
  const [state, setState] = useState({
    agents: [
    ],
    loading: false,
    message: "",
    messageType: "",
  });

  useEffect(() => {
    getAgents()
  }, []);

  const getAgents = () => {
    setState({
      ...state,
      message: "",
      loading: true,
    });
    RestService.getAgents().then(
      (res) => {
        setState({
          ...state,
          agents: res.data,
          loading: false,
        });
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

          setState({
            ...state,
            loading: false,
            message: resMessage,
          });
      }
    );
  }

  const deleteAgent = (id) => {
    setState({
      ...state,
      message: "",
      loading: true,
    });
    RestService.deleteAgent(id).then(
      (res) => {
        console.log("res", res);
        let a = state.agents.filter(d => d.id !== id)
        setState({
          ...state,
          agents: a,
          message: "Agent deleted successfully",
          messageType: 'success',
          processing: false,
          loading: false,
        });
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setState({
          ...state,
          loading: false,
          message: resMessage,
          processing: false,
        });
      }
    );
  }


  return (
    <CRow>
      <CCol lg={12}>
        <CCard className="m-2 p-4">
          <div className="flex flex-col">
            <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
              <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div className="overflow-hidden">
                  <table className="min-w-full divide-y divide-gray-200">
                    <thead className="bg-gray-50">
                      <tr>
                      <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          S/N
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          First Name
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Last Name
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Username
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Email
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Country
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Phone Number
                        </th>
                        <th scope="col" className="relative px-6 py-3">
                          <span className="sr-only">Edit</span>
                        </th>
                        <th scope="col" className="relative px-6 py-3">
                          <span className="sr-only">Delete</span>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {state.agents.length > 0 &&
                        state.agents.map((agent, agentIdx) => (
                          <tr
                            key={agent.id}
                            className={
                              agentIdx % 2 === 0 ? "bg-white" : "bg-gray-50"
                            }
                          >
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-600 capitalize">
                              {agentIdx + 1}
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-600 capitalize">
                              {agent.first_name}
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-600 capitalize">
                              {agent.last_name}
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-600">
                              {agent.username}
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-600">
                              {agent.email}
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-600 capitalize">
                              {agent.country}
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-600 capitalize">
                              {agent.call_code + agent.phone_number}
                            </td>
                            <td className=" whitespace-nowrap text-right text-sm font-medium w-0 px-4">
                              <div className="flex justify-end">
                                <div
                                  onClick={() => history.push(`/agents/edit-agent/${agent.id}`)}
                                  className="bg-indigo-400 hover:bg-indigo-500 text-center py-2 w-16 rounded-md cursor-pointer"
                                >
                                  Edit
                                </div>
                              </div>
                            </td>
                            <td className=" whitespace-nowrap text-right text-sm font-medium w-0">
                              <div className="flex justify-end">
                                <div
                                  onClick={() => deleteAgent(agent.id)}
                                  className="bg-red-400 hover:bg-red-500 text-center py-2 w-16 rounded-md cursor-pointer"
                                >
                                  Delete
                                </div>
                              </div>
                            </td>
                          </tr>
                        ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          {state.agents.length === 0 &&
            state.selectedUserId &&
            !state.loading && (
              <div className="p-6 text-center">User has no agent</div>
          )}

          <br />
          {state.message && !state.messageType && (
            <div className="form-group">
              <div className="alert alert-danger" role="alert">
                {state.message}
              </div>
            </div>
          )}
        </CCard>
      </CCol>
    </CRow>
  );
};

export default Agents;
