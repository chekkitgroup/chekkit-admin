import React, { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";

import {
  CCard,
  CCol,
  CRow,
  CLabel,
  CSelect,
  CFormGroup,
  CInput,
} from "@coreui/react";
import RestService from "../../_services/rest.service";

const CreateEditAgent = () => {
  const {id} = useParams()
  const history = useHistory()
  const [state, setState] = useState({
    currentAgent: {},
    loading: false,
    message: "",
    messageType: "",
  });

  useEffect(() => {
    if (id) {
      fetchAgent();
    }
  }, [id]);

  const handleInputChange = (e) => {
    const {name, value} = e.target;
    setState({
      ...state,
      currentAgent: {...state.currentAgent, [name]: value}
    })
  }

  const onSubmit = () => {
    setState({
      ...state,
      message: "",
      processing: true,
      loading: true,
    });
    if (id && state.currentAgent.first_name) {
      updateAgent(state.currentAgent)
    } else {
      createAgent(state.currentAgent)
    }
  };

  const fetchAgent = () => {
    setState({
      ...state,
      message: "",
      loading: true,
    });
    RestService.getAgent(id).then(
      (res) => {
        console.log("res", res);
        setState({
          ...state,
          currentAgent: {...res.data, phone_number: '0'+ res.data.phone_number},
          message: "Agent retrieved successfully",
          messageType: 'success',
          processing: false,
          loading: false,
        });
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setState({
          ...state,
          loading: false,
          message: resMessage,
          processing: false,
        });
      }
    );
  }

  const updateAgent = (data) => {
    RestService.updateAgent(id, data).then(
      (res) => {
        console.log("res", res);
        setState({
          ...state,
          message: "Agent updated successfully",
          messageType: 'success',
          processing: false,
          loading: false,
        });
        setTimeout(() => {
          history.push('/agents')
        }, 2000);
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setState({
          ...state,
          loading: false,
          message: resMessage,
          processing: false,
        });
      }
    );
  }

  const createAgent = (data) => {
    RestService.createAgent(data).then(
      (res) => {
        console.log("res", res);
        setState({
          ...state,
          message: "Agent created successfully",
          messageType: 'success',
          processing: false,
          loading: false,
        });
        setTimeout(() => {
          history.push('/agents')
        }, 2000);
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setState({
          ...state,
          loading: false,
          message: resMessage,
          processing: false,
        });
      }
    );
  }


  return (
    <CRow>
      <CCol lg={12}>
        <CCard className="m-2 p-4">

          <CFormGroup>
            <CLabel>First Name</CLabel>
            <CInput
              type="text"
              name="first_name"
              placeholder="First Name"
              onChange={handleInputChange}
              value={state.currentAgent.first_name}
            />
          </CFormGroup>

          <CFormGroup>
            <CLabel>Last Name</CLabel>
            <CInput
              type="text"
              name="last_name"
              placeholder="Last Name"
              onChange={handleInputChange}
              value={state.currentAgent.last_name}
            />
          </CFormGroup>

          <CFormGroup>
            <CLabel>Username</CLabel>
            <CInput
              type="text"
              name="username"
              placeholder="Username"
              onChange={handleInputChange}
              value={state.currentAgent.username}
            />
          </CFormGroup>

          <CFormGroup>
            <CLabel>Email</CLabel>
            <CInput
              type="email"
              name="email"
              placeholder="Email"
              onChange={handleInputChange}
              value={state.currentAgent.email}
            />
          </CFormGroup>

          <CFormGroup>
            <CLabel>Phone Number</CLabel>
            <CInput
              type="number"
              name="phone_number"
              placeholder="Phone Number"
              onChange={handleInputChange}
              value={state.currentAgent.phone_number}
            />
          </CFormGroup>

          {!id && <CFormGroup>
            <CLabel>Password</CLabel>
            <CInput
              type="password"
              name="password"
              placeholder="Password"
              onChange={handleInputChange}
              value={state.currentAgent.password}
            />
          </CFormGroup>}


          <button
            onClick={onSubmit}
            className="btn btn-success btn-block"
            disabled={state.loading || state.processing}
          >
            {(state.loading || state.processing) && (
              <span className="spinner-border spinner-border-sm"></span>
            )}
            <span>SUBMIT</span>
          </button>
          <br />
          {state.message && (
            <div className="form-group">
              <div className={`alert ${state.messageType == `success` ? `alert-success` : `alert-danger`}`} role="alert">
                {state.message}
              </div>
            </div>
          )}
        </CCard>
      </CCol>
    </CRow>
  );
};

export default CreateEditAgent;
