import React, { Component, useState, useEffect } from 'react'

import { useHistory, useLocation } from 'react-router-dom'
import {
  CBadge,
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
  CPagination
} from '@coreui/react'

import RestService from '../../_services/rest.service';
import Moment from 'moment';

const getBadge = status => {
  switch (status) {
    case 'Active': return 'success'
    case 'Inactive': return 'secondary'
    case 'Pending': return 'warning'
    case 'Banned': return 'danger'
    default: return 'primary'
  }
}
// const [modal, setModal] = useState(true)

export default class Users extends Component {
  constructor(props) {
    super(props);
    // this.handleLogin = this.handleLogin.bind(this);
    // this.onChangeUsername = this.onChangeUsername.bind(this);
    // this.onChangePassword = this.onChangePassword.bind(this);
    // const history = useHistory()

    // const pageChange = newPage => {
    //   currentPage !== newPage && history.push(`/users?page=${newPage}`)
    // }
    // const [modal, setModal] = useState(true)
    this.setModal = this.setModal.bind(this);
    this.state = {
      modal:false,
      users: [],
      passedData: undefined,
      currentPage: "",
      password: "",
      loading: false,
      message: ""
    };
  }

  componentDidMount() {
    this.getReports()
    // this.setPage()
  }

  getReports() {

    this.setState({
      message: "",
      loading: true
    });

    // this.form.validateAll();

      RestService.getProductReports().then(
        (res) => {
          // console.log(res.data)

          res.data.forEach(function(entry) {
            // console.log(entry);
                entry.created_at = Moment(entry.created_at).format('D-MM-yyyy')
            });
            this.setState({
              users: res.data,
            });
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            loading: false,
            message: resMessage
          });
        }
      );
  }
  setModal(data){
      console.log(data)
      this.setState({
        modal: !this.state.modal,
        passedData: data
      });
    // this.modal = !this.modal;
  }

  setPage(){
    const queryPage = useLocation().search.match(/page=([0-9]+)/, '')
    // const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1)
    // const [page, setPage] = useState(currentPage)
  }
  useHistoryCall(){    
    const history = useHistory()
    return history;
  }
  pageChange(newPage) {
    this.state.currentPage !== newPage && this.history.push(`/users?page=${newPage}`)
  }
// const Users = () => {

  // useEffect(() => {
  //   currentPage !== page && setPage(currentPage)
  // }, [currentPage, page])
 
  // fields={[
  //   { key: 'name', _classes: 'font-weight-bold' },
  //   'registered', 'role', 'status'
  // ]}
  render() {
    var _this = this;
    if(this.state.users) {          
      // console.log(this.state.users)
    //   <CDropdownItem onClick={() => _this.logout()}>

      return (
        <CRow>
            <CModal 
                show={_this.state.modal} 
                // onClose={setModal}
                >
                <CModalHeader closeButton>
                    <CModalTitle>View Report</CModalTitle>
                </CModalHeader>
                <CModalBody>
                <p className="h4">{this.state.passedData?this.state.passedData.product_name:''}</p> <br/>
                <p><b>Experience:</b> {this.state.passedData?this.state.passedData.product_user_exprnce:''}</p>
                <p><b>Store Address:</b> {this.state.passedData?this.state.passedData.product_store_address:''}</p>

                <a target='_blank' href={this.state.passedData?this.state.passedData.product_image:''}>
                    <img style={{height: '100px'}} src={this.state.passedData?this.state.passedData.product_image:''} />
                </a>
                </CModalBody>
                <CModalFooter>
                    <CButton 
                    color="secondary" 
                    onClick={() => _this.setModal()} 
                    >Close</CButton>
                </CModalFooter>
            </CModal>

          <CCol xl={12}>
            <CCard>
              <CCardHeader>
                Reports
              </CCardHeader>
              <CCardBody>
              <CDataTable
                items={this.state.users}
                fields={[
                  { key: 'product_name', _classes: 'font-weight-bold' },{label: 'Date', key: 'created_at'}
                ]}
                hover
                striped
                itemsPerPage={100}
                activePage={this.page}
                clickableRows
                onRowClick={(item) => _this.setModal(item)}
                scopedSlots = {{
                  'status':
                    (item)=>(
                      <td>
                        <CBadge color={getBadge(item.status)}>
                          {item.status}
                        </CBadge>
                      </td>
                    )
                }}
              />
              <CPagination
                activePage={this.page}
                onActivePageChange={this.pageChange}
                // pages={5}
                doubleArrows={false} 
                align="center"
              />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      )
    }
  } 

}
