import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { CSVLink, CSVDownload } from "react-csv";

import {
  CCard,
  CCol,
  CRow,
  CPagination,
  CLabel,
  CSelect,
  CFormGroup,
  CContainer,
  CForm,
  CInputCheckbox,
  CInput,
  CCardBody,
} from "@coreui/react";
import Button from "../../reusable/button";
import RestService from "../../_services/rest.service";

const GenerateReport = () => {
  const [state, setState] = useState({
    selectedUserId: "",
    selectedProductId: "",
    selectedBatchId: "",
    from: "",
    to: "",
    users: [],
    batches: "",
    products: "",
    report: "",
    quest: [],
    loading: false,
    editing: false,
    message: "",
    messageType: "",
  });

  useEffect(() => {
    getUsers();
  }, []);

  const getUsers = () => {
    setState({
      ...state,
      message: "",
      loading: true,
    });
    RestService.getAllUsers().then(
      (res) => {
        setState({
          ...state,
          users: res.data,
          loading: false,
        });
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setState({
          ...state,
          loading: false,
          message: resMessage,
        });
      }
    );
  };

  const onUserChange = (e) => {
    setState({
      ...state,
      selectedUserId: e.target.value,
    });
  };

  const onProductIdChange = (e) => {
    setState({
      ...state,
      selectedProductId: e.target.value,
    });
  };

  const onBatchIdChange = (e) => {
    setState({
      ...state,
      selectedBatchId: e.target.value,
    });
  };

  const onFromChange = (e) => {
    setState({
      ...state,
      from: e.target.value,
    });
  };

  const onToChange = (e) => {
    setState({
      ...state,
      to: e.target.value,
    });
  };

  useEffect(() => {
    if (state.selectedUserId) {
      setState({
        ...state,
        message: "",
        loading: true,
      });
      RestService.getUserProducts(state.selectedUserId).then(
        (res) => {
          setState({
            ...state,
            products: res.data.products,
            loading: false,
          });
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
        }
      );
    }
  }, [state.selectedUserId]);

  useEffect(() => {
    if (state.selectedProductId) {
      setState({
        ...state,
        message: "",
        loading: true,
      });
      RestService.getProductBatches(state.selectedProductId).then(
        (res) => {
          setState({
            ...state,
            batches: res.data.productBatches,
            loading: false,
          });
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
        }
      );
    }
  }, [state.selectedProductId]);

  const generateReport = () => {
    setState({
      ...state,
      message: "",
      processing: true,
      loading: true,
    });
    RestService.genrateReport(state.from, state.to, state.selectedBatchId).then(
      (res) => {
        console.log("report", res);
        setState({
          ...state,
          report: res.data,
          message: "Report generated successfully",
          processing: false,
          loading: false,
        });
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setState({
          ...state,
          loading: false,
          message: resMessage,
          processing: false,
        });
      }
    );
  };

  useEffect(() => {
    if (state.report) {
      getQuestData();
    }
  }, [state.report]);

  const getQuestData = () => {
    let x = state.report.quest_response.map((d, i) => {
      d.option_count.map((e, j) => {
        d[`answer${j + 1}`] = e.answer;
        d[`count${j + 1}`] = e.count;
        return e;
      });
      return d;
    });
    setState({ ...state, quest: x });
  };

  return (
    <CRow>
      <CCol lg={12}>
        <CCard className="m-2 p-4">
          <CFormGroup>
            <CLabel htmlFor="ccaccount" className="m-2">
              Select Account
            </CLabel>
            <CSelect
              custom
              name="ccaccount"
              id="ccaccount"
              onChange={onUserChange}
              value={state.selectedUserId}
            >
              <option value="">Select your option</option>
              {state.users &&
                state.users.map((item) => (
                  <option key={item.id} value={item.id}>
                    {item.company_rep
                      ? item.company_rep
                      + " " + '('+ ' ' + item.username + ' ' +')'
                      : "" + " - " + item.first_name
                      ? item.first_name
                      : ""}
                  </option>
                ))}
            </CSelect>
          </CFormGroup>
          <CFormGroup>
            <CLabel htmlFor="ccaccount" className="m-2">
              Select Product
            </CLabel>
            <CSelect
              custom
              name="ccaccount"
              id="ccaccount"
              onChange={onProductIdChange}
              value={state.selectedProductId}
            >
              <option value="user_id">Select your option</option>
              {state.products &&
                state.products.length > 0 &&
                state.products.map((item) => (
                  <option key={item.id} value={item.id}>
                    {item.product_name ? item.product_name : item.producer_name}
                  </option>
                ))}
            </CSelect>
          </CFormGroup>
          <CFormGroup>
            <CLabel htmlFor="ccaccount" className="m-2">
              Select Batch
            </CLabel>
            <CSelect
              custom
              name="ccaccount"
              id="ccaccount"
              onChange={onBatchIdChange}
              value={state.selectedBatchId}
            >
              <option value="user_id">Select your option</option>
              {state.batches &&
                state.batches.length > 0 &&
                state.batches.map((item) => (
                  <option key={item.id} value={item.id}>
                    {item.product_name ? item.product_name : item.batch_num}
                  </option>
                ))}
            </CSelect>
          </CFormGroup>
          <CFormGroup>
            <CLabel>From Date</CLabel>
            <CInput type="date" onChange={onFromChange} value={state.from} />
          </CFormGroup>
          <CFormGroup>
            <CLabel>To Date</CLabel>
            <CInput type="date" onChange={onToChange} value={state.to} />
          </CFormGroup>
          {state.selectedUserId &&
            state.selectedProductId &&
            state.selectedBatchId &&
            state.from &&
            state.to && (
              <button
                onClick={generateReport}
                className="btn btn-success btn-block"
                disabled={state.loading || state.processing}
              >
                {(state.loading || state.processing) && (
                  <span className="spinner-border spinner-border-sm"></span>
                )}
                <span>GENERATE REPORT</span>
              </button>
            )}
          <br />
          {state.message && !state.messageType && (
            <div className="form-group">
              <div className="alert alert-danger" role="alert">
                {state.message}
              </div>
            </div>
          )}
          {state.report && (
            <>
              {/* <CSVDownload
                headers={[
                  { label: "PRODUCT NAME", key: "product_name" },
                  { label: "DATE", key: "date" },
                  { label: "COUNT", key: "count" },
                ]}
                data={state.report.stats.map((d) => {
                  d.product_name = state.report.product.product_name;
                  return d;
                })}
                target="_blank"
              /> */}
              <div className="flex flex-col justify-center items-center lg:flex-row space-y-6 lg:space-y-0 lg:space-x-6">
                <CSVLink
                  className="bg-blue-400 px-4 py-2 w-full text-center rounded no-underline block"
                  headers={[
                    { label: "PRODUCT NAME", key: "product_name" },
                    { label: "BATCH NUMBER", key: "batch_num" },
                    { label: "DATE", key: "date" },
                    { label: "COUNT", key: "count" },
                  ]}
                  data={state.report.stats.map((d) => {
                    d.product_name = state.report.product.product_name;
                    d.batch_num = state.report.batch.batch_num;
                    return d;
                  })}
                >
                  Download Scan Report
                </CSVLink>
                {state.quest.length > 0 && (
                  <CSVLink
                    className="bg-blue-400 px-4 py-2 w-full text-center rounded no-underline block"
                    headers={[
                      { label: "PRODUCT NAME", key: "product_name" },
                      { label: "BATCH NUMBER", key: "batch_num" },
                      { label: "QUESTION", key: "question" },
                      { label: `ANSWER 1`, key: `answer1` },
                      { label: `COUNT 1`, key: `count1` },
                      { label: `ANSWER 2`, key: `answer2` },
                      { label: `COUNT 2`, key: `count2` },
                      { label: `ANSWER 3`, key: `answer3` },
                      { label: `COUNT 3`, key: `count3` },
                      { label: `ANSWER 4`, key: `answer4` },
                      { label: `COUNT 4`, key: `count4` },
                    ]}
                    data={state.quest.map((d) => {
                      d.product_name = state.report.product.product_name;
                      d.batch_num = state.report.batch.batch_num;
                      return d;
                    })}
                  >
                    Download Survey Report
                  </CSVLink>
                )}
                <CSVLink
                  className="bg-blue-400 px-4 py-2 w-full text-center rounded no-underline block"
                  headers={[
                    { label: "PRODUCT NAME", key: "product_name" },
                    { label: "BATCH NUMBER", key: "batch_num" },
                    { label: "DATE", key: "date" },
                    { label: "COUNT", key: "count" },
                  ]}
                  data={state.report.reward_count.map((d) => {
                    d.product_name = state.report.product.product_name;
                    d.batch_num = state.report.batch.batch_num;
                    return d;
                  })}
                >
                  Download Reward Report
                </CSVLink>
              </div>
            </>
          )}
        </CCard>
      </CCol>
    </CRow>
  );
};

export default GenerateReport;
