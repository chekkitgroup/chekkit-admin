import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

import {
  CCard,
  CCol,
  CRow,
  CPagination,
  CLabel,
  CSelect,
  CFormGroup,
  CInputCheckbox,
  CInput,
} from "@coreui/react";
import Button from "../../reusable/button";
import RestService from "../../_services/rest.service";
import Survey from "../../reusable/survey-edit";

const ViewSurveys = () => {
  const [state, setState] = useState({
    selectedUserId: "",
    currentPage: "",
    users: [],
    batches: [],
    products: "",
    rewards: "",
    surveys: "",
    loading: false,
    editing: false,
    currentSurvey: "",
    surveyData: "",
    message: "",
    messageType: "",
  });

  useEffect(() => {
    getUsers();
  }, []);

  const getUsers = () => {
    setState({
      ...state,
      message: "",
      loading: true,
    });
    RestService.getAllUsers().then(
      (res) => {
        setState({
          ...state,
          users: res.data,
          loading: false,
        });
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setState({
          ...state,
          loading: false,
          message: resMessage,
        });
      }
    );
  };

  const onUserChange = (e) => {
    setState({
      ...state,
      selectedUserId: e.target.value,
    });
  };

  const onProductNameChange = (e) => {
    setState({
      ...state,
      currentSurvey: { ...state.currentSurvey, product_name: e.target.value },
    });
  };

  const onExpiryChange = (e) => {
    setState({
      ...state,
      currentSurvey: { ...state.currentSurvey, expiry_date: e.target.value },
    });
  };

  const onProductionChange = (e) => {
    setState({
      ...state,
      currentSurvey: { ...state.currentSurvey, production_date: e.target.value },
    });
  };

  const onSurveyChange = (e) => {
    setState({
      ...state,
      currentSurvey: { ...state.currentSurvey, surveyId: e.target.value },
    });
  };

  const onRewardChange = (e) => {
    setState({
      ...state,
      currentSurvey: { ...state.currentSurvey, rewardId: e.target.value },
    });
  };

  const onProductIdChange = (e) => {
    setState({
      ...state,
      currentSurvey: { ...state.currentSurvey, productId: e.target.value },
    });
  };

  const onFeaturedChange = (e) => {
    setState({
      ...state,
      currentSurvey: { ...state.currentSurvey, featured: e.target.checked },
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    setState({
      ...state,
      message: "",
      loading: true,
    });

    let formData = new FormData();
    formData.append("productId", state.currentSurvey.productId);
    formData.append("product_name", state.currentSurvey.product_name);
    formData.append("featured", state.currentSurvey.featured);
    formData.append("expiry_date", state.currentSurvey.expiry_date);
    formData.append("production_date", state.currentSurvey.production_date);
    formData.append("surveyId", state.currentSurvey.surveyId);
    formData.append("rewardId", state.currentSurvey.rewardId);
    formData.append("userId", state.selectedUserId);

    RestService.updateBatch(
      state.currentSurvey.batch_num,
      state.currentSurvey
    ).then(
      () => {
        setState({
          ...state,
          message: "Update successful",
          loading: false,
        });
        RestService.getBatches(state.selectedUserId).then((res) => {
          setState({
            ...state,
            batches: res.data.batch,
          });
        });
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setState({
          ...state,
          loading: false,
          message: resMessage,
        });
      }
    );
  };

  useEffect(() => {
    if (state.selectedUserId) {
      setState({
        ...state,
        message: "",
        loading: true,
      });
      Promise.all([
        RestService.getUserSurveys(state.selectedUserId),
        RestService.getBatches(state.selectedUserId),
        RestService.getUserProducts(state.selectedUserId),
        RestService.getUserRewards(state.selectedUserId),
      ]).then(
        (res) => {
          console.log("RES !!", res);
          setState({
            ...state,
            surveys: res[0].data.surveys,
            batches: res[1].data.batch,
            products: res[2].data.products,
            rewards: res[3].data.rewards,
            loading: false,
          });
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
        }
      );
    }
  }, [state.selectedUserId]);

  useEffect(() => {
    if (state.currentSurvey) {
      RestService.getSurveyQuestions(
        state.currentSurvey.id
      ).then(
        (res) => {
          setState({
            ...state,
            surveyData: {survey: state.currentSurvey, question: res.data}
          });
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          setState({
            ...state,
            loading: false,
            message: resMessage,
          });
        }
      );
    }
  }, [state.currentSurvey])

  const refetch = () => {
    RestService.getUserSurveys(state.selectedUserId).then(
      (res) => {
        setState({
          ...state,
          currentSurvey: "",
          editing: false,
          surveys: res.data.surveys,
        });
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setState({
          ...state,
          loading: false,
          message: resMessage,
        });
      }
    );
  }

  return (
    <CRow>
      <CCol lg={12}>
        <CCard className="m-2 p-4">
          {!state.editing && (
            <>
              <CFormGroup>
                <CLabel htmlFor="ccaccount" className="m-2">
                  Select Account
                </CLabel>
                <CSelect
                  custom
                  name="ccaccount"
                  id="ccaccount"
                  onChange={onUserChange}
                  value={state.selectedUserId}
                >
                  <option value="">Select your option</option>
                  {state.users &&
                    state.users.map((item) => (
                      <option key={item.id} value={item.id}>
                        {item.company_rep
                          ? item.company_rep+ " " + '('+ ' ' + item.username + ' ' +')'
                          : "" + " - " + item.first_name
                          ? item.first_name
                          : ""}
                      </option>
                    ))}
                </CSelect>
              </CFormGroup>
              <div className="flex flex-col">
                <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                  <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div className="overflow-hidden">
                      <table className="min-w-full divide-y divide-gray-200">
                        <thead className="bg-gray-50">
                          <tr>
                            <th
                              scope="col"
                              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            >
                              Survey Title
                            </th>
                            <th
                              scope="col"
                              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            >
                              Survey Description
                            </th>
                            <th
                              scope="col"
                              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            >
                              Survey Slug
                            </th>
                            <th
                              scope="col"
                              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            >
                              Created Date
                            </th>
                            <th scope="col" className="relative px-6 py-3">
                              <span className="sr-only">Edit</span>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {state.surveys &&
                            state.surveys.map((survey, surveyIdx) => (
                              <tr
                                key={survey.slug}
                                className={
                                  surveyIdx % 2 === 0
                                    ? "bg-white"
                                    : "bg-gray-50"
                                }
                              >
                                <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                  {survey.title}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                  {survey.content}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                  {survey.slug}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                  {new Date(
                                    survey.created_at
                                  ).toLocaleDateString()}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                  <div
                                    onClick={() =>
                                      setState({
                                        ...state,
                                        editing: true,
                                        currentSurvey: survey,
                                      })
                                    }
                                    className="text-indigo-600 hover:text-indigo-900 cursor-pointer"
                                  >
                                    Edit
                                  </div>
                                </td>
                              </tr>
                            ))}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              {state.surveys.length === 0 &&
                state.selectedUserId &&
                !state.loading && (
                  <div className="p-6 text-center">User has no surveys</div>
                )}
            </>
          )}
          {state.editing && (
            <div>
              <Survey
                data={state.surveyData}
                cb={(a) => {
                  if (a) {
                    refetch();
                  } else {
                    setState({
                      ...state,
                      editing: false,
                    });
                  }
                }
                }
              />
            </div>
          )}
        </CCard>
      </CCol>
    </CRow>
  );
};

export default ViewSurveys;
