import React from "react";

import Survey from "../../reusable/survey-edit";

const CreateSurvey = () => {
  return (
    <div>
      <Survey />
    </div>
  );
};

export default CreateSurvey;
