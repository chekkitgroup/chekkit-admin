import React, { Component } from "react";

import {
  CRow,
  CContainer,
  CForm,
  CFormGroup,
  CInputCheckbox,
  CLabel,
  CCard,
  CInput,
  CCardBody,
  CCol,
  CSelect,
} from "@coreui/react";
import RestService from "../../_services/rest.service";

const rewardTypes = [
  { id: 1, name: "Merchandize" },
  { id: 2, name: "Raffle" },
  { id: 3, name: "Airtime" },
];

export default class Users extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.resetForm = this.resetForm.bind(this);
    this.onRewardTypeChange = this.onRewardTypeChange.bind(this);
    this.onRewardQuantityChange = this.onRewardQuantityChange.bind(this);
    this.onRewardValueChange = this.onRewardValueChange.bind(this);
    this.onUserIdChange = this.onUserIdChange.bind(this);

    this.state = {
      reward_type: "",
      reward_value: "",
      reward_quant: "",
      user_id: "",
      users: [],
      processing: false,
      loading: false,
      message: "",
      messageType: "",
    };
  }

  componentDidMount() {
    this.getUsers();
  }

  getUsers() {
    this.setState({
      message: "",
      loading: true,
    });
    RestService.getAllUsers().then(
      (res) => {
        this.setState({
          users: res.data,
          loading: false,
        });
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        this.setState({
          loading: false,
          message: resMessage,
        });
      }
    );
  }

  onRewardTypeChange(e) {
    this.setState({
      reward_type: e.target.value,
    });
  }

  onRewardQuantityChange(e) {
    this.setState({
      reward_quant: e.target.value,
    });
  }

  onRewardValueChange(e) {
    this.setState({
      reward_value: e.target.value,
    });
  }

  onUserIdChange(e) {
    this.setState({
      user_id: e.target.value,
    });
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({
      message: "",
      processing: true,
    });

    let data = {
      user_id: this.state.user_id,
      rewards: [
        {
          reward_type: this.state.reward_type,
          reward_value: this.state.reward_value,
          reward_quant: this.state.reward_quant,
        },
      ],
    };

    // let formData = new FormData();
    // formData.append("rewards", data);
    // // formData.append("user_id", this.state.user_id);

    // console.log(formData);

    RestService.createSurveyReward(data).then(
      () => {
        this.resetForm();
        this.setState({
          message: "Update successful",
          processing: false,
        });
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        this.setState({
          loading: false,
          message: resMessage,
          processing: false,
        });
      }
    );
  }

  resetForm() {
    this.setState({
      reward_type: "",
      reward_value: "",
      reward_quant: "",
      user_id: "",
    });
  }

  render() {
    if (this.state.users) {
      return (
        <CContainer>
          <CRow>
            <CCol xs="10" sm="10" md="10" lg="10">
              <CCard className="mx-4">
                <CCardBody className="p-4">
                  <CForm onSubmit={this.handleSubmit}>
                    <CFormGroup>
                      <CLabel htmlFor="ccaccount" className="m-2">
                        Select Account
                      </CLabel>
                      <CSelect
                        custom
                        name="ccaccount"
                        id="ccaccount"
                        onChange={this.onUserIdChange}
                        value={this.state.user_id}
                      >
                        <option value="user_id">Select your option</option>
                        {this.state.users &&
                          this.state.users.map((item) => (
                            <option key={item.id} value={item.id}>
                              {item.company_rep
                                ? item.company_rep+ " " + '('+ ' ' + item.username + ' ' +')'
                                : "" + " - " + item.first_name
                                ? item.first_name
                                : ""}
                            </option>
                          ))}
                      </CSelect>
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel htmlFor="ccaccount" className="m-2">
                        Reward Type
                      </CLabel>
                      <CSelect
                        custom
                        name="ccaccount"
                        id="ccaccount"
                        onChange={this.onRewardTypeChange}
                        value={this.state.reward_type}
                      >
                        <option value="user_id">Select your option</option>
                        {rewardTypes.map((item) => (
                          <option key={item.id} value={item.name}>
                            {item.name}
                          </option>
                        ))}
                      </CSelect>
                    </CFormGroup>

                    <CRow>
                      <CCol xs="6">
                        <CFormGroup>
                          <CLabel htmlFor="nf-name">Reward Quantity</CLabel>
                          <CInput
                            type="number"
                            name="nf-number"
                            placeholder="Reward Quantity"
                            value={this.state.reward_quant}
                            onChange={this.onRewardQuantityChange}
                          />
                        </CFormGroup>
                      </CCol>
                      <CCol xs="6">
                        <CFormGroup>
                          <CLabel htmlFor="nf-name">Reward Value</CLabel>
                          <CInput
                            type="number"
                            placeholder="Reward Value"
                            value={this.state.reward_value}
                            onChange={this.onRewardValueChange}
                          />
                        </CFormGroup>
                      </CCol>
                    </CRow>

                    <button
                      className="btn btn-success btn-block"
                      disabled={this.state.loading || this.state.processing}
                    >
                      {(this.state.loading || this.state.processing) && (
                        <span className="spinner-border spinner-border-sm"></span>
                      )}
                      <span>SAVE AND UPDATE</span>
                    </button>
                    <br />
                    {this.state.message && !this.state.messageType && (
                      <div className="form-group">
                        <div className="alert alert-danger" role="alert">
                          {this.state.message}
                        </div>
                      </div>
                    )}
                    {this.state.message && this.state.messageType && (
                      <div className="form-group">
                        <div className="alert alert-info" role="alert">
                          {this.state.message}
                        </div>
                      </div>
                    )}
                  </CForm>
                </CCardBody>
              </CCard>
            </CCol>
          </CRow>
        </CContainer>
      );
    }
  }
}
