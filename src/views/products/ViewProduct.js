import React, { Component, useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { CCard, CCardBody, CCardHeader, CCol, CRow } from '@coreui/react'

import CIcon from '@coreui/icons-react'
import RestService from '../../_services/rest.service';
import Moment from 'moment';



const User = ({match}) => {
    const user = products.find( user => user.id.toString() === match.params.id)
    const userDetails = user ? Object.entries(user) : 
      [['id', (<span><CIcon className="text-muted" name="cui-icon-ban" /> Not found</span>)]]


export default class Users extends Component {
    constructor(props) {
        super(props);
    this.getProductDetails = this.getProductDetails.bind(this)
        
        this.state = {
          modal:false,
          products: [],
        };
      }

      componentDidMount() {
        this.getUsers()
        this.getAllProducts()
        this.getProductDetails()
      }
    
      getProductDetails(event) {
        this.setState({
            selectedUserId: event.target.value
        });
        console.log(event.target.value)
        let id = event.target.value;
        this.setState({
        message: "",
        loading: true
        });
        RestService.getProductDetails(id).then(
        (res) => {
            // console.log(res.data)

            this.setState({
                products: res.data.products,
                loading: false
            });

            console.log(this.state.products)
        },
        error => {
            const resMessage =
            (error.response &&
                error.response.data &&
                error.response.data.message) ||
            error.message ||
            error.toString();

            this.setState({
            loading: false,
            message: resMessage
            });
        }
        );
    }

    
    
    

        render() {
            var _this = this;
            if(this.state.users) {          
            // console.log(this.state.users)
            //   <CDropdownItem onClick={() => _this.logout()}>

        return (
            <CRow>
            <CCol lg={6}>
              <CCard>
                <CCardHeader>
                  User id: {match.params.id}
                </CCardHeader>
                <CCardBody>
                    <table className="table table-striped table-hover">
                      <tbody>
                        {
                          getProductDetails.map(([key, value], index) => {
                            return (
                              <tr key={index.toString()}>
                                <td>{`${key}:`}</td>
                                <td><strong>{value}</strong></td>
                              </tr>
                            )
                          })
                        }
                      </tbody>
                    </table>
                </CCardBody>
              </CCard>
            </CCol>
          </CRow>
    )
}
}
}

}
