import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

import {
  CCard,
  CCol,
  CRow,
  CPagination,
  CLabel,
  CSelect,
  CFormGroup,
  CContainer,
  CForm,
  CInputCheckbox,
  CInput,
  CCardBody,
} from "@coreui/react";
import Button from "../../reusable/button";
import RestService from "../../_services/rest.service";

const ViewSubProducts = () => {
  const [state, setState] = useState({
    selectedUserId: "",
    currentPage: "",
    users: [],
    batches: [],
    products: "",
    rewards: "",
    surveys: "",
    loading: false,
    editing: false,
    currentBatch: "",
    message: "",
    messageType: "",
  });

  useEffect(() => {
    getUsers();
  }, []);

  const getUsers = () => {
    setState({
      ...state,
      message: "",
      loading: true,
    });
    RestService.getAllUsers().then(
      (res) => {
        setState({
          ...state,
          users: res.data,
          loading: false,
        });
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setState({
          ...state,
          loading: false,
          message: resMessage,
        });
      }
    );
  };

  const onUserChange = (e) => {
    setState({
      ...state,
      selectedUserId: e.target.value,
    });
  };

  const onUploadChange = (e) => {
    setState({
      ...state,
      currentBatch: { ...state.currentBatch, photo: e.target.files[0] },
    });
  }

  const onProductNameChange = (e) => {
    setState({
      ...state,
      currentBatch: { ...state.currentBatch, product_name: e.target.value },
    });
  };

  const onExpiryChange = (e) => {
    setState({
      ...state,
      currentBatch: { ...state.currentBatch, expiry_date: e.target.value },
    });
  };

  const onProductionChange = (e) => {
    setState({
      ...state,
      currentBatch: { ...state.currentBatch, production_date: e.target.value },
    });
  };

  const onSurveyChange = (e) => {
    setState({
      ...state,
      currentBatch: { ...state.currentBatch, survey_id: e.target.value },
    });
  };

  const onRewardChange = (e) => {
    setState({
      ...state,
      currentBatch: { ...state.currentBatch, reward_id: e.target.value },
    });
  };

  const onProductIdChange = (e) => {
    setState({
      ...state,
      currentBatch: { ...state.currentBatch, productId: e.target.value },
    });
  };

  const onFeaturedChange = (e) => {
    setState({
      ...state,
      currentBatch: { ...state.currentBatch, featured: e.target.checked },
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    setState({
      ...state,
      message: "",
      loading: true,
    });

    let formData = new FormData();
    formData.append("productId", state.currentBatch.productId);
    formData.append("product_name", state.currentBatch.product_name);
    formData.append("featured", state.currentBatch.featured);
    formData.append("expiry_date", state.currentBatch.expiry_date);
    formData.append("production_date", state.currentBatch.production_date);
    formData.append("survey_id", state.currentBatch.surveyId);
    formData.append("reward_id", state.currentBatch.rewardId);
    formData.append("user_id", state.selectedUserId);

    RestService.updateBatch(
      state.currentBatch.batch_num,
      state.currentBatch
    ).then(
      () => {
        setState({
          ...state,
          message: "Update successful",
          loading: false,
        });
        RestService.getBatches(state.selectedUserId).then((res) => {
          setState({
            ...state,
            batches: res.data.batch,
          });
        });
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setState({
          ...state,
          loading: false,
          message: resMessage,
        });
      }
    );
  };

  useEffect(() => {
    if (state.selectedUserId) {
      setState({
        ...state,
        message: "",
        loading: true,
      });
      Promise.all([
        RestService.getBatches(state.selectedUserId),
        RestService.getUserProducts(state.selectedUserId),
        RestService.getUserRewards(state.selectedUserId),
        RestService.getUserSurveys(state.selectedUserId),
      ]).then(
        (res) => {
          console.log("RES !!", res);
          setState({
            ...state,
            batches: res[0].data.batch,
            products: res[1].data.products,
            rewards: res[2].data.rewards,
            surveys: res[3].data.surveys,
            loading: false,
          });
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
        }
      );
    }
  }, [state.selectedUserId]);

  return (
    <CRow>
      <CCol lg={12}>
        <CCard className="m-2 p-4">
          {!state.editing && (
            <>
              <CFormGroup>
                <CLabel htmlFor="ccaccount" className="m-2">
                  Select Account
                </CLabel>
                <CSelect
                  custom
                  name="ccaccount"
                  id="ccaccount"
                  onChange={onUserChange}
                  value={state.selectedUserId}
                >
                  <option value="">Select your option</option>
                  {state.users &&
                    state.users.map((item) => (
                      <option key={item.id} value={item.id}>
                        {item.company_rep
                          ? item.company_rep+ " " + '('+ ' ' + item.username + ' ' +')'
                          : "" + " - " + item.first_name
                          ? item.first_name
                          : ""}
                      </option>
                    ))}
                </CSelect>
              </CFormGroup>
              <div className="flex flex-col">
                <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                  <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div className="overflow-hidden">
                      <table className="min-w-full divide-y divide-gray-200">
                        <thead className="bg-gray-50">
                          <tr>
                            <th
                              scope="col"
                              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            >
                              Product Name
                            </th>
                            <th
                              scope="col"
                              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            >
                              Batch Number
                            </th>
                            <th
                              scope="col"
                              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            >
                              Prod Date
                            </th>
                            <th
                              scope="col"
                              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            >
                              Expiry Date
                            </th>
                            <th
                              scope="col"
                              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            >
                              Featured
                            </th>
                            <th scope="col" className="relative px-6 py-3">
                              <span className="sr-only">Edit</span>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {state.batches &&
                            state.batches.map((batch, batchIdx) => (
                              <tr
                                key={batch.batch_num}
                                className={
                                  batchIdx % 2 === 0 ? "bg-white" : "bg-gray-50"
                                }
                              >
                                <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                  {batch.product_name}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                  {batch.batch_num}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                  {new Date(
                                    batch.production_date
                                  ).toLocaleDateString()}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                  {new Date(
                                    batch.expiry_date
                                  ).toLocaleDateString()}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                  {batch.featured + ""}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                  <div
                                    onClick={() =>
                                      setState({
                                        ...state,
                                        editing: true,
                                        currentBatch: {
                                          ...batch,
                                          reward_id: batch.rewardId,
                                          survey_id: batch.surveyId,
                                        },
                                      })
                                    }
                                    className="text-indigo-600 hover:text-indigo-900 cursor-pointer"
                                  >
                                    Edit
                                  </div>
                                </td>
                              </tr>
                            ))}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              {state.batches.length === 0 &&
                state.selectedUserId &&
                !state.loading && (
                  <div className="p-6 text-center">User has no batch</div>
                )}
            </>
          )}
          {state.editing && (
            <div>
              <div>
                <CFormGroup>
                  <CLabel htmlFor="ccaccount" className="m-2">
                    Select Product
                  </CLabel>
                  <CSelect
                    custom
                    name="ccaccount"
                    id="ccaccount"
                    onChange={onProductIdChange}
                    value={state.currentBatch.productId}
                  >
                    <option value="user_id">Select your option</option>
                    {state.products &&
                      state.products.length > 0 &&
                      state.products.map((item) => (
                        <option key={item.id} value={item.id}>
                          {item.product_name
                            ? item.product_name
                            : item.producer_name}
                        </option>
                      ))}
                  </CSelect>
                </CFormGroup>
                <CFormGroup>
                  <CLabel>Upload Product Image</CLabel>
                  <CInput
                    type="file"
                    placeholder="No File Chosen"
                    onChange={onUploadChange}
                  />
                </CFormGroup>
                <CFormGroup>
                  <CLabel>Product Name</CLabel>
                  <CInput
                    type="name"
                    placeholder="Product Name"
                    onChange={onProductNameChange}
                    value={state.currentBatch.product_name}
                  />
                </CFormGroup>
                {/* <CFormGroup>
                  <CLabel>Producer Name</CLabel>
                  <CInput
                    type="text"
                    placeholder="Producer Name"
                    onChange={onProducerNameChange}
                    value={state.currentBatch.producer_name}
                  />
                </CFormGroup> */}

                <CFormGroup>
                  <CLabel>Production Date</CLabel>
                  <CInput
                    type="date"
                    onChange={onProductionChange}
                    value={state.currentBatch.production_date}
                  />
                </CFormGroup>
                <CFormGroup>
                  <CLabel>Expiry Date</CLabel>
                  <CInput
                    type="date"
                    onChange={onExpiryChange}
                    value={state.currentBatch.expiry_date}
                  />
                </CFormGroup>
                <CFormGroup>
                  <CLabel htmlFor="ccaccount" className="m-2">
                    Please select the survey you want to attach to this Product:
                  </CLabel>
                  <CSelect
                    custom
                    name="ccaccount"
                    id="ccaccount"
                    onChange={onSurveyChange}
                    value={state.currentBatch.survey_id}
                  >
                    <option value="user_id">Select your option</option>
                    {state.surveys &&
                      state.surveys.length > 0 &&
                      state.surveys.map((item) => (
                        <option key={item.id} value={item.id}>
                          {item.title ? item.title : item.content}
                        </option>
                      ))}
                  </CSelect>
                </CFormGroup>

                <CFormGroup>
                  <CLabel htmlFor="ccaccount" className="m-2">
                    Please select the reward type to attach to this Product:
                  </CLabel>
                  <CSelect
                    custom
                    name="ccaccount"
                    id="ccaccount"
                    onChange={onRewardChange}
                    value={state.currentBatch.reward_id}
                  >
                    <option value="user_id">Select your option</option>
                    {state.rewards &&
                      state.rewards.length > 0 &&
                      state.rewards.map((item) => (
                        <option key={item.id} value={item.id}>
                          {item.reward_type
                            ? item.reward_type
                            : item.reward_type}
                        </option>
                      ))}
                  </CSelect>
                </CFormGroup>

                <div
                  style={{
                    display: "flex",
                    marginLeft: "20px",
                    marginTop: "20px",
                    marginBottom: "20px",
                  }}
                >
                  <CLabel>Featured Batch</CLabel>
                  <CInputCheckbox
                    type="checkbox"
                    onChange={onFeaturedChange}
                    checked={state.currentBatch.featured}
                    value={state.currentBatch.featured}
                  />
                </div>
                <div className={`w-full flex justify-between items-end pt-6`}>
                  <div>
                    <Button
                      text={`Back`}
                      type={`secondary`}
                      size={2}
                      onClick={() =>
                        setState({
                          ...state,
                          editing: false,
                          currentBatch: "",
                          message: "",
                          messageType: "",
                        })
                      }
                    />
                  </div>
                  <div
                    className={`flex items-end lg:items-center flex-col space-y-6 lg:flex-row lg:space-y-0  lg:space-x-6`}
                  >
                    <Button text={`Update`} size={2} onClick={handleSubmit} />
                  </div>
                </div>

                <br />
                {state.message && !state.messageType && (
                  <div className="form-group">
                    <div className="alert alert-danger" role="alert">
                      {state.message}
                    </div>
                  </div>
                )}
                {state.message && state.messageType && (
                  <div className="form-group">
                    <div className="alert alert-info" role="alert">
                      {state.message}
                    </div>
                  </div>
                )}
              </div>
            </div>
          )}
        </CCard>
      </CCol>
    </CRow>
  );
};

export default ViewSubProducts;
