import React, { Component, useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'

import {
    CCard,
    CCardBody,
    CCardHeader,
    CButtonGroup,
    CButton,
    CCol,
    CRow,
    CPagination,
    CBadge,
    CDataTable,
    CDropdown,
    CDropdownItem,
    CDropdownMenu,
    CDropdownToggle,
    CModal,
    CModalBody,
    CModalHeader,
    CLabel,
    CSelect,
    CFormGroup,
    CModalFooter } from '@coreui/react'
import CIcon from '@coreui/icons-react'
import RestService from '../../_services/rest.service';
import Moment from 'moment';


    const getBadge = status => {
        switch (status) {
        case true: return 'success'
        case false: return 'secondary'
        case 'Pending': return 'warning'
        case 'Banned': return 'danger'
        default: return 'primary'
        }
    }

export default class Users extends Component {
    constructor(props) {
        super(props);
        this.getUserProducts = this.getUserProducts.bind(this);
        this.getAllProducts = this.getAllProducts.bind(this);
        this.getProductBatches = this.getProductBatches.bind(this);
        this.onStartChange = this.onStartChange.bind(this);
        this.onEndChange = this.onEndChange.bind(this);
        this.onBatchChange = this.onBatchChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.resetForm = this.resetForm.bind(this);
        // const history = useHistory()

        this.state = {
          modal:false,
          selectedUserId: '',
          selectedProdId: '',
          selectedBatchId: '',
          start: '',
          end: '',
          users: [],
          passedData: undefined,
          currentPage: "",
          products: [],
          productBatches: [],
          processing: false,
          loading: false,
          message: "",
          messageType: "",
          history: undefined,
        };
      }

      componentDidMount() {
        this.getUsers()
        this.getAllProducts()
      }

        getUsers() {
            this.setState({
                message: "",
                loading: true
            });
            RestService.getAllUsers().then(
                (res) => {
                    // console.log('USERS',res.data)
                    this.setState({
                        users: res.data,
                        loading: false
                    });
                    // this.state = {users: res.data};
                    // console.log(this.state.users)
                    // this.props.history.push("/profile");
                    // window.location.reload();
                },
                error => {
                    const resMessage =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();

                    this.setState({
                    loading: false,
                    message: resMessage
                    });
                }
            );
        }

        setPage(){
            console.log('newPage')
            const queryPage = useLocation().search.match(/page=([0-9]+)/, '')
            const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1)
            // const [page, setPage] = useState(currentPage)
          }


        getAllProducts() {
            this.setState({
            message: "",
            loading: true
            });
            RestService.getProducts(0,100,1).then(
            (res) => {
                console.log(res.data)
                this.setState({
                    products: res.data.products.rows,
                    loading: false
                });
                console.log(this.state.products)
            },
            error => {
                const resMessage =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

                this.setState({
                loading: false,
                message: resMessage
                });
            }
            );
        }



        getUserProducts(event) {
            this.setState({
                selectedUserId: event.target.value
            });
            console.log(event.target.value)
            let id = event.target.value;
            this.setState({
            message: "",
            loading: true
            });
            RestService.getUserProducts(id).then(
            (res) => {
                // console.log(res.data)

                this.setState({
                    products: res.data.products,
                    loading: false
                });

                console.log(this.state.products)
            },
            error => {
                const resMessage =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

                this.setState({
                loading: false,
                message: resMessage
                });
            }
            );
        }

        getProductBatches(event) {
            console.log(event.target.value)
            let id = event.target.value;
            this.setState({
            message: "",
            selectedProdId: event.target.value,
            loading: true
            });

            RestService.getProductBatches(id).then(
            (res) => {
                this.setState({
                    productBatches: res.data.productBatches,
                    loading: false
                });

                console.log(this.state.productBatches)
            },
            error => {
                const resMessage =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

                this.setState({
                loading: false,
                message: resMessage
                });
            }
            );
        }


        onBatchChange(e) {
            this.setState({
                selectedBatchId: e.target.value
            });
            console.log(e.target.value)
        }

        onStartChange(e) {
            this.setState({
                start: e.target.value
            });
        }

        onEndChange(e) {
            this.setState({
                end: e.target.value
            });
        }

        handleSubmit(e) {
          e.preventDefault();

          this.setState({
            message: "",
            processing: true
          });

          var data = {
                selectedUserId: this.state.selectedUserId,
                selectedProdId: this.state.selectedProdId,
                selectedBatchId: this.state.selectedBatchId,
                start: this.state.start,
                end: this.state.end,
            }

          console.log(data)
          // this.form.validateAll();

        }
        resetForm(){
            this.setState({
                selectedUserId: '',
                selectedProdId: '',
                selectedBatchId: '',
                start: '',
                end: '',
            });
        }

        //     const [modal, setModal] = useState(false);

        //   const toggle = ()=>{
        //     setModal(!modal);
        //   }
        render() {
            var _this = this;
            if(this.state.users) {
            // console.log(this.state.users)
            //   <CDropdownItem onClick={() => _this.logout()}>

        return (
            <CRow>
                {/* Modal */}
                {/* <CModal show={modal} onClose={toggle}>
                    <CModalHeader closeButton> <strong>Are you sure?</strong></CModalHeader>
                    <CModalFooter>
                    <CButton color="primary">Yes</CButton>{' '}
                    <CButton color="secondary" onClick={toggle}>No</CButton>
                    </CModalFooter>
                </CModal> */}
            <CCol lg={12}>
                <CCard className="m-2">
                    <CFormGroup>
                        <CLabel htmlFor="ccaccount" className="m-2">Select Account</CLabel>
                            <CSelect custom name="ccaccount" id="ccaccount" onChange={this.getUserProducts} value={this.state.selectedUserId}>
                                <option value="">Select your option</option>
                                    {this.state.users.map(item => (
                                        <option key={item.id} value={item.id}>
                                                {item.company_rep?item.company_rep+ " " + '('+ ' ' + item.username + ' ' +')':'' + ' - ' + item.first_name?item.first_name:''}
                                        </option>
                                        ))}
                            </CSelect>
                    </CFormGroup>
                </CCard>
            </CCol>

          <CCol lg={12}>
            <CCard>
              <CCardBody>
                <CDataTable
                    items={this.state.products}
                    fields={[
                        {label: '', key: 'photo'},
                        { key: 'product_name', _classes: 'font-weight-bold' },
                        {label: 'Producer Name', key: 'producer_name'},
                        {label: 'Date Added', key: 'created_at'},
                        {label: 'Actions', key: 'but'}
                    ]} hover striped itemsPerPage={100} activePage={this.page} clickableRows
                    // onRowClick={(item) => _this.setModal(item)}
                        scopedSlots = {

                        {
                            'but':
                                (item)=>(
                                <td>
                                    <CButtonGroup>
                                <CButton color="success" className="m-1" onClick={() => this.props.history.push(`/users/${item.id}`)}>View</CButton>
                                        <CButton color="info" className="m-1">Edit</CButton>
                                        <CButton color="danger" className="m-1" >Delete</CButton>
                                    </CButtonGroup>
                                </td>
                        ),

                        'photo':
                            (item)=>(
                            <td>
                                <img style={{height: '100px'}} src={item.product_image?item.product_image:''} />
                            </td>
                            )
                        }
                    }
                />
                {/* <CPagination activePage={this.page} onActivePageChange={this.pageChange} // pages={5}
                doubleArrows={false}  align="center"
                /> */}
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
    )
}
}
}
