import React, { Component } from "react";

import {
  CRow,
  CContainer,
  CForm,
  CFormGroup,
  CInputCheckbox,
  CLabel,
  CCard,
  CInput,
  CCardBody,
  CCol,
  CSelect,
} from "@coreui/react";
import RestService from "../../_services/rest.service";

export default class Users extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.resetForm = this.resetForm.bind(this);
    this.onProductNameChange = this.onProductNameChange.bind(this);
    this.onProducerNameChange = this.onProducerNameChange.bind(this);
    this.onFdaNumberChange = this.onFdaNumberChange.bind(this);
    this.onBatchNumberChange = this.onBatchNumberChange.bind(this);
    this.onExpiryChange = this.onExpiryChange.bind(this);
    this.onRewardChange = this.onRewardChange.bind(this);
    this.onSurveyChange = this.onSurveyChange.bind(this);
    this.onProductionChange = this.onProductionChange.bind(this);
    this.onUserIdChange = this.onUserIdChange.bind(this);
    this.onProductIdChange = this.onProductIdChange.bind(this);
    this.onFeaturedChange = this.onFeaturedChange.bind(this);

    this.state = {
      productId: "",
      product_name: "",
      producer_name: "",
      featured: false,
      expiry_date: "",
      production_date: "",
      batch_num: "",
      FDA_number: "",
      reward_id: "",
      survey_id: "",
      user_id: "",
      products: "",
      rewards: "",
      surveys: "",
      users: [],
      processing: false,
      loading: false,
      message: "",
      messageType: "",
    };
  }

  componentDidMount() {
    this.getUsers();
  }

  componentDidUpdate() {
    if (
      this.state.user_id &&
      !this.state.surveys &&
      !this.state.rewards &&
      !this.state.products
    ) {
      Promise.all([
        RestService.getUserProducts(this.state.user_id),
        RestService.getUserRewards(this.state.user_id),
        RestService.getUserSurveys(this.state.user_id),
      ]).then(
        (res) => {
          console.log("RES !!", res);
          this.setState({
            products: res[0].data.products,
            rewards: res[1].data.rewards,
            surveys: res[2].data.surveys,
            loading: false,
          });
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
        }
      );
    }
  }

  getUsers() {
    this.setState({
      message: "",
      loading: true,
    });
    RestService.getAllUsers().then(
      (res) => {
        this.setState({
          users: res.data,
          loading: false,
        });
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        this.setState({
          loading: false,
          message: resMessage,
        });
      }
    );
  }

  onProductNameChange(e) {
    this.setState({
      product_name: e.target.value,
    });
  }

  onProducerNameChange(e) {
    this.setState({
      producer_name: e.target.value,
    });
  }

  onFdaNumberChange(e) {
    this.setState({
      FDA_number: e.target.value,
    });
  }

  onBatchNumberChange(e) {
    this.setState({
      batch_num: e.target.value,
    });
  }

  onExpiryChange(e) {
    this.setState({
      expiry_date: e.target.value,
    });
  }

  onProductionChange(e) {
    this.setState({
      production_date: e.target.value,
    });
  }

  onSurveyChange(e) {
    this.setState({
      survey_id: e.target.value,
    });
  }

  onRewardChange(e) {
    this.setState({
      reward_id: e.target.value,
    });
  }

  onUserIdChange(e) {
    this.setState({
      user_id: e.target.value,
      products: null,
      rewards: null,
      surveys: null,
    });
  }

  onProductIdChange(e) {
    this.setState({
      productId: e.target.value,
    });
  }

  onFeaturedChange(e) {
    console.log(e.target.checked);
    this.setState({
      featured: e.target.checked,
    });
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({
      message: "",
      processing: true,
    });

    let formData = new FormData();
    formData.append("productId", this.state.productId);
    formData.append("product_name", this.state.product_name);
    formData.append("producer_name", this.state.producer_name);
    formData.append("featured", this.state.featured);
    formData.append("batch_num", this.state.batch_num);
    formData.append("expiry_date", this.state.expiry_date);
    formData.append("production_date", this.state.production_date);
    formData.append("FDA_number", this.state.FDA_number);
    formData.append("survey_id", this.state.survey_id);
    formData.append("reward_id", this.state.reward_id);
    formData.append("user_id", this.state.user_id);

    // var data = {
    //   product_name: this.state.product_name,
    //   photo: this.state.photo,
    //   producer_name: this.state.producer_name,
    //   barcode_num: this.state.barcode_num,
    //   batch_num: this.state.batch_num,
    //   id_range: this.state.id_range,
    //   user_id: this.state.user_id,
    //   expiry_date: this.state.expiry_date,
    //   production_date: this.state.production_date,
    //   product_category: this.state.product_category,
    //   FDA_number: this.state.FDA_number,
    //   survey_id: this.state.survey_id,
    //   reward_id: this.state.reward_id,
    // };
    console.log(formData);

    RestService.createBatch(formData).then(
      () => {
        this.resetForm();
        this.setState({
          message: "Update successful",
          processing: false,
        });
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        this.setState({
          loading: false,
          message: resMessage,
          processing: false,
        });
      }
    );
  }

  resetForm() {
    this.setState({
      productId: "",
      product_name: "",
      producer_name: "",
      featured: false,
      expiry_date: "",
      production_date: "",
      batch_num: "",
      FDA_number: "",
      reward_id: "",
      survey_id: "",
      user_id: "",
    });
  }

  render() {
    if (this.state.users) {
      return (
        <CContainer>
          <CRow>
            <CCol xs="10" sm="10" md="10" lg="10">
              <CCard className="mx-4">
                <CCardBody className="p-4">
                  <CForm onSubmit={this.handleSubmit}>
                    <CFormGroup>
                      <CLabel htmlFor="ccaccount" className="m-2">
                        Select Account
                      </CLabel>
                      <CSelect
                        custom
                        name="ccaccount"
                        id="ccaccount"
                        onChange={this.onUserIdChange}
                        value={this.state.user_id}
                      >
                        <option value="user_id">Select your option</option>
                        {this.state.users &&
                          this.state.users.map((item) => (
                            <option key={item.id} value={item.id}>
                              {item.company_rep
                                ? item.company_rep+ " " + '('+ ' ' + item.username + ' ' +')'
                                : "" + " - " + item.first_name
                                ? item.first_name
                                : ""}
                            </option>
                          ))}
                      </CSelect>
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel htmlFor="ccaccount" className="m-2">
                        Select Product
                      </CLabel>
                      <CSelect
                        custom
                        name="ccaccount"
                        id="ccaccount"
                        onChange={this.onProductIdChange}
                        value={this.state.productId}
                      >
                        <option value="user_id">Select your option</option>
                        {this.state.products &&
                          this.state.products.length > 0 &&
                          this.state.products.map((item) => (
                            <option key={item.id} value={item.id}>
                              {item.product_name
                                ? item.product_name
                                : item.producer_name}
                            </option>
                          ))}
                      </CSelect>
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel>Batch Name</CLabel>
                      <CInput
                        type="name"
                        placeholder="Product Name"
                        onChange={this.onProductNameChange}
                        value={this.state.product_name}
                      />
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel>Producer Name</CLabel>
                      <CInput
                        type="text"
                        placeholder="Producer Name"
                        onChange={this.onProducerNameChange}
                        value={this.state.producer_name}
                      />
                    </CFormGroup>

                    <CFormGroup>
                      <CLabel>Production Date</CLabel>
                      <CInput
                        type="date"
                        onChange={this.onProductionChange}
                        value={this.state.production_date}
                      />
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel>Expiry Date</CLabel>
                      <CInput
                        type="date"
                        onChange={this.onExpiryChange}
                        value={this.state.expiry_date}
                      />
                    </CFormGroup>
                    <CRow>
                      <CCol xs="6">
                        <CFormGroup>
                          <CLabel htmlFor="nf-name">FDA Number:</CLabel>
                          <CInput
                            type="number"
                            name="nf-number"
                            placeholder="FDA Number"
                            value={this.state.FDA_number}
                            onChange={this.onFdaNumberChange}
                          />
                        </CFormGroup>
                      </CCol>
                      <CCol xs="6">
                        <CFormGroup>
                          <CLabel htmlFor="nf-name">Batch Number</CLabel>
                          <CInput
                            type="number"
                            placeholder="Product Batch Number"
                            value={this.state.batch_num}
                            onChange={this.onBatchNumberChange}
                          />
                        </CFormGroup>
                      </CCol>
                    </CRow>
                    <CFormGroup>
                      <CLabel htmlFor="ccaccount" className="m-2">
                        Please select the survey you want to attach to this
                        Product:
                      </CLabel>
                      <CSelect
                        custom
                        name="ccaccount"
                        id="ccaccount"
                        onChange={this.onSurveyChange}
                        value={this.state.survey_id}
                      >
                        <option value="user_id">Select your option</option>
                        {this.state.surveys &&
                          this.state.surveys.length > 0 &&
                          this.state.surveys.map((item) => (
                            <option key={item.id} value={item.id}>
                              {item.title ? item.title : item.content}
                            </option>
                          ))}
                      </CSelect>
                    </CFormGroup>

                    <CFormGroup>
                      <CLabel htmlFor="ccaccount" className="m-2">
                        Please select the reward type to attach to this Product:
                      </CLabel>
                      <CSelect
                        custom
                        name="ccaccount"
                        id="ccaccount"
                        onChange={this.onRewardChange}
                        value={this.state.reward_id}
                      >
                        <option value="user_id">Select your option</option>
                        {this.state.rewards &&
                          this.state.rewards.length > 0 &&
                          this.state.rewards.map((item) => (
                            <option key={item.id} value={item.id}>
                              {item.reward_type
                                ? item.reward_type
                                : item.reward_type}
                            </option>
                          ))}
                      </CSelect>
                    </CFormGroup>

                    <div
                      style={{
                        display: "flex",
                        marginLeft: "20px",
                        marginTop: "20px",
                        marginBottom: "20px",
                      }}
                    >
                      <CLabel>Featured Batch</CLabel>
                      <CInputCheckbox
                        type="checkbox"
                        onChange={this.onFeaturedChange}
                        value={this.state.featured}
                      />
                    </div>

                    <button
                      className="btn btn-success btn-block"
                      disabled={this.state.loading || this.state.processing}
                    >
                      {(this.state.loading || this.state.processing) && (
                        <span className="spinner-border spinner-border-sm"></span>
                      )}
                      <span>SAVE AND UPDATE</span>
                    </button>
                    <br />
                    {this.state.message && !this.state.messageType && (
                      <div className="form-group">
                        <div className="alert alert-danger" role="alert">
                          {this.state.message}
                        </div>
                      </div>
                    )}
                    {this.state.message && this.state.messageType && (
                      <div className="form-group">
                        <div className="alert alert-info" role="alert">
                          {this.state.message}
                        </div>
                      </div>
                    )}
                  </CForm>
                </CCardBody>
              </CCard>
            </CCol>
          </CRow>
        </CContainer>
      );
    }
  }
}
