import React, { Component, useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";

import {
  CRow,
  CContainer,
  CForm,
  CFormGroup,
  CLabel,
  CCard,
  CInput,
  CCardBody,
  CCol,
  CSelect,
  CTextarea,
} from "@coreui/react";
import RestService from "../../_services/rest.service";

import CIcon from "@coreui/icons-react";

export default class Users extends Component {
  constructor(props) {
    super(props);
    this.getUserProducts = this.getUserProducts.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.resetForm = this.resetForm.bind(this);
    this.onProductNameChange = this.onProductNameChange.bind(this);
    this.onProducerNameChange = this.onProducerNameChange.bind(this);
    this.onTextAreaChange = this.onTextAreaChange.bind(this);
    this.onFdaNumberChange = this.onFdaNumberChange.bind(this);
    this.onBatchNumberChange = this.onBatchNumberChange.bind(this);
    this.onBarCodeChange = this.onBarCodeChange.bind(this);
    this.onAgeGroupChange = this.onAgeGroupChange.bind(this);
    this.onUploadChange = this.onUploadChange.bind(this);
    this.onExpiryChange = this.onExpiryChange.bind(this);
    this.onRewardChange = this.onRewardChange.bind(this);
    this.onProductChange = this.onProductChange.bind(this);
    this.onSurveyChange = this.onSurveyChange.bind(this);
    this.onRangeChange = this.onRangeChange.bind(this);
    this.onProductionChange = this.onProductionChange.bind(this);
    this.onUserIdChange = this.onUserIdChange.bind(this);
    this.getBase64 = this.getBase64.bind(this);

    this.state = {
      modal: false,
      product_name: "",
      producer_name: "",
      product_description: "",
      age_range: "",
      photo: {},
      barcode_num: "",
      batch_num: "",
      FDA_number: "",
      passedData: undefined,
      currentPage: "",
      products: [],
      rewards: "",
      surveys: "",
      users: [],
      productBatches: [],
      processing: false,
      loading: false,
      message: "",
      messageType: "",
      reward_id: "",
      survey_id: "",
      user_id: "",
      selectedUserId: "",
      product_category: "",
      expiry_date: "",
      production_date: "",
    };
  }

  componentDidMount() {
    this.getUsers();
  }

  componentDidUpdate() {
    if (this.state.user_id && !this.state.surveys && !this.state.rewards) {
      Promise.all([
        RestService.getUserRewards(this.state.user_id),
        RestService.getUserSurveys(this.state.user_id),
      ]).then(
        (res) => {
          this.setState({
            rewards: res[0].data.rewards,
            surveys: res[1].data.surveys,
            loading: false,
          });
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
        }
      );
    }
  }

  getUsers() {
    this.setState({
      message: "",
      loading: true,
    });
    RestService.getAllUsers().then(
      (res) => {
        // console.log(res.data)
        this.setState({
          users: res.data,
          loading: false,
        });
        // this.state = {users: res.data};
        // console.log(this.state.users)
        // this.props.history.push("/profile");
        // window.location.reload();
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        this.setState({
          loading: false,
          message: resMessage,
        });
      }
    );
  }

  getUserProducts(event) {
    this.setState({
      selectedUserId: event.target.value,
      user_id: event.target.value,
    });
    console.log(event.target.value);
    let id = event.target.value;
    this.setState({
      message: "",
      loading: true,
    });
    RestService.getUserProducts(id).then(
      (res) => {
        // console.log(res.data)

        this.setState({
          products: res.data.products,
          loading: false,
        });

        console.log(this.state.products);
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        this.setState({
          loading: false,
          message: resMessage,
        });
      }
    );
  }

  onProductNameChange(e) {
    this.setState({
      product_name: e.target.value,
    });
    // console.log(e.target.value)
  }

  onAgeGroupChange(e) {
    this.setState({
      age_range: e.target.value,
    });
    // console.log(e.target.value)
  }

  onProducerNameChange(e) {
    this.setState({
      producer_name: e.target.value,
    });
    // console.log(e.target.value)
  }

  onFdaNumberChange(e) {
    this.setState({
      FDA_number: e.target.value,
    });
    // console.log(e.target.value)
  }

  onTextAreaChange(e) {
    this.setState({
      product_description: e.target.value,
    });
    // console.log(e.target.value)
  }

  onBatchNumberChange(e) {
    this.setState({
      batch_num: e.target.value,
    });
    // console.log(e.target.value)
  }

  onBarCodeChange(e) {
    this.setState({
      barcode_num: e.target.value,
    });
    // console.log(e.target.value)
  }

  onUploadChange(e) {
    this.setState({
      photo: e.target.files[0],
    });
    // this.getBase64(e.target.files[0], (buffer) => {
    //   this.setState({
    //     photo: buffer,
    //   });
    // });
    // console.log(e.target.files);
  }

  onExpiryChange(e) {
    this.setState({
      expiry_date: e.target.value,
    });
    // console.log(e.target.value)
  }

  onProductionChange(e) {
    this.setState({
      production_date: e.target.value,
    });
    // console.log(e.target.value)
  }

  onProductChange(e) {
    this.setState({
      product_category: e.target.value,
    });
    // console.log(e.target.value)
  }

  onSurveyChange(e) {
    this.setState({
      survey_id: e.target.value,
    });
    // console.log(e.target.value)
  }

  onRewardChange(e) {
    this.setState({
      reward_id: e.target.value,
    });
    // console.log(e.target.value)
  }

  onRangeChange(e) {
    this.setState({
      id_range: e.target.value,
    });
    // console.log(e.target.value)
  }

  onUserIdChange(e) {
    this.setState({
      user_id: e.target.value,
    });
    // console.log(e.target.value)
  }

  setPage() {
    console.log("newPage");
    const queryPage = useLocation().search.match(/page=([0-9]+)/, "");
    const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1);
    // const [page, setPage] = useState(currentPage)
  }

  getBase64(file, cb) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      console.log(reader.result);
      cb(reader.result);
    };
    reader.onerror = function (error) {
      console.log("Error: ", error);
      return file;
    };
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({
      message: "",
      processing: true,
    });

    let formData = new FormData();
    formData.append("product_name", this.state.product_name);
    formData.append("photo", this.state.photo);
    formData.append("producer_name", this.state.producer_name);
    formData.append("product_description", this.state.product_description);
    formData.append("barcode_num", this.state.barcode_num);
    formData.append("batch_num", this.state.batch_num);
    formData.append("age_range", this.state.age_range);
    formData.append("id_range", this.state.id_range);
    formData.append("user_id", this.state.user_id);
    formData.append("expiry_date", this.state.expiry_date);
    formData.append("production_date", this.state.production_date);
    formData.append("product_category", this.state.product_category);
    formData.append("FDA_number", this.state.FDA_number);
    formData.append("survey_id", this.state.survey_id);
    formData.append("reward_id", this.state.reward_id);

    // formData.append("product_name", "SODA & SOFT DRINK");
    // formData.append("photo", this.state.photo);
    // formData.append("producer_name", "Chekkit Marketing");
    // formData.append(
    //   "product_description",
    //   "Chekkit survey for food and drinks"
    // );
    // formData.append("barcode_num", "123");
    // formData.append("batch_num", "123");
    // formData.append("age_range", "20-29 years");
    // formData.append("id_range", "undefined");
    // formData.append("user_id", 1);
    // formData.append("expiry_date", "2026-05-14T00:00:00.000Z");
    // formData.append("production_date", "2021-12-12T00:00:00.000Z");
    // formData.append("product_category", "Synthesized");
    // formData.append("FDA_number", "123");
    // formData.append("survey_id", this.state.survey_id);
    // formData.append("reward_id", this.state.reward_id);

    console.log(formData);
    // this.form.validateAll();

    RestService.addProducts(formData).then(
      () => {
        this.resetForm();
        this.setState({
          message: "Update successful",
          processing: false,
        });
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        this.setState({
          loading: false,
          message: resMessage,
          processing: false,
        });
      }
    );
  }

  resetForm() {
    this.setState({
      productName: "",
      producerName: "",
      textArea: "",
      ageGroup: "",
      fda: "",
      batchNumber: "",
      upload: [],
      user_id: "",
      production_date: "",
    });
  }
  render() {
    var _this = this;
    if (this.state.users) {
      return (
        <CContainer>
          <CRow>
            <CCol xs="10" sm="10" md="10" lg="10">
              <CCard className="mx-4">
                <CCardBody className="p-4">
                  <CForm onSubmit={this.handleSubmit}>
                    <CFormGroup>
                      <CLabel htmlFor="ccaccount" className="m-2">
                        Select Account
                      </CLabel>
                      <CSelect
                        custom
                        name="ccaccount"
                        id="ccaccount"
                        onChange={this.getUserProducts}
                        value={this.state.selectedUserId}
                      >
                        <option value="user_id">Select your option</option>
                        {this.state.users.map((item) => (
                          <option key={item.id} value={item.id}>
                            {item.company_rep
                              ? item.company_rep + " " + '('+ ' ' + item.username + ' ' +')'
                              : "" + " - " + item.first_name
                              ? item.first_name
                              : ""}
                          </option>
                        ))}
                      </CSelect>
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel>Upload Product Image</CLabel>
                      <CInput
                        type="file"
                        placeholder="No File Chosen"
                        onChange={this.onUploadChange}
                        // value={this.state.photo}
                      />
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel>Product Name</CLabel>
                      <CInput
                        type="name"
                        placeholder="Product Name"
                        onChange={this.onProductNameChange}
                        value={this.state.product_name}
                      />
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel>Producer Name</CLabel>
                      <CInput
                        type="text"
                        placeholder="Producer Name"
                        onChange={this.onProducerNameChange}
                        value={this.state.producer_name}
                      />
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel>Product Description</CLabel>
                      <CTextarea
                        rows="7"
                        placeholder="Product Description"
                        onChange={this.onTextAreaChange}
                        value={this.state.product_description}
                      />
                    </CFormGroup>
                    <CRow>
                      <CCol xs="4">
                        <CFormGroup>
                          <CLabel>What Age Group is the Product For:</CLabel>
                          <CSelect
                            custom
                            name="ccaccount"
                            id="ccaccount"
                            onChange={this.onAgeGroupChange}
                            value={this.state.age_range}
                          >
                            Select your option
                            <option>0-2 years</option>
                            <option>2-12 years</option>
                            <option>13-19 years</option>
                            <option>20-29 years</option>
                            <option>30-40 years</option>
                            <option>40 and above</option>
                          </CSelect>
                        </CFormGroup>
                      </CCol>
                    </CRow>
                    <CFormGroup>
                      <CLabel>Production Date</CLabel>
                      <CInput
                        type="date"
                        onChange={this.onProductionChange}
                        value={this.state.production_date}
                      />
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel>Expiry Date</CLabel>
                      <CInput
                        type="date"
                        onChange={this.onExpiryChange}
                        value={this.state.expiry_date}
                      />
                    </CFormGroup>
                    <CRow>
                      <CCol xs="4">
                        <CFormGroup>
                          <CLabel htmlFor="nf-name">FDA Number:</CLabel>
                          <CInput
                            type="number"
                            name="nf-number"
                            placeholder="FDA Number"
                            value={this.state.FDA_number}
                            onChange={this.onFdaNumberChange}
                          />
                        </CFormGroup>
                      </CCol>
                      <CCol xs="4">
                        <CFormGroup>
                          <CLabel htmlFor="nf-name">Batch Number</CLabel>
                          <CInput
                            type="number"
                            placeholder="Product Batch Number"
                            value={this.state.batch_num}
                            onChange={this.onBatchNumberChange}
                          />
                        </CFormGroup>
                      </CCol>
                      <CCol xs="4">
                        <CFormGroup>
                          <CLabel htmlFor="nf-name">Barcode Number</CLabel>
                          <CInput
                            type="number"
                            placeholder="Barcode Number"
                            value={this.state.barcode_num}
                            onChange={this.onBarCodeChange}
                          />
                        </CFormGroup>
                      </CCol>
                    </CRow>
                    <CFormGroup>
                      <CLabel>Product Category:</CLabel>
                      <CSelect
                        custom
                        name="ccaccount"
                        id="ccaccount"
                        onChange={this.onProductChange}
                        value={this.state.product_category}
                      >
                        Select your option
                        <option>Organic</option>
                        <option>Synthesized</option>
                        <option>Hybrid</option>
                      </CSelect>
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel>ID Range:</CLabel>
                      <CSelect
                        custom
                        name="ccaccount"
                        id="ccaccount"
                        onChange={this.onRangeChange}
                        value={this.state.id_range}
                      >
                        Select your option
                        <option>10,000 units</option>
                        <option>50,000 units</option>
                        <option>500,000 units</option>
                      </CSelect>
                    </CFormGroup>
                    {/* <CFormGroup>
                      <CLabel>
                        Please select the survey you want to attach to this
                        Product:
                      </CLabel>
                      <CSelect
                        onChange={this.onSurveyChange}
                        value={this.state.survey_id}
                      >
                        Select your option
                        <option>1</option>
                        <option>2</option>
                      </CSelect>
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel>
                        Please select the reward type to attach to this Product:
                      </CLabel>
                      <CSelect
                        onChange={this.onRewardChange}
                        value={this.state.reward_id}
                      >
                        Select your option
                        <option>Loyalty Points</option>
                        <option>Airtime/Merchandize</option>
                      </CSelect>
                    </CFormGroup> */}

                    <CFormGroup>
                      <CLabel htmlFor="ccaccount" className="m-2">
                        Please select the survey you want to attach to this
                        Product:
                      </CLabel>
                      <CSelect
                        custom
                        name="ccaccount"
                        id="ccaccount"
                        onChange={this.onSurveyChange}
                        value={this.state.survey_id}
                      >
                        <option value="user_id">Select your option</option>
                        {this.state.surveys &&
                          this.state.surveys.length > 0 &&
                          this.state.surveys.map((item) => (
                            <option key={item.id} value={item.id}>
                              {item.title ? item.title : item.content}
                            </option>
                          ))}
                      </CSelect>
                    </CFormGroup>

                    <CFormGroup>
                      <CLabel htmlFor="ccaccount" className="m-2">
                        Please select the reward type to attach to this Product:
                      </CLabel>
                      <CSelect
                        custom
                        name="ccaccount"
                        id="ccaccount"
                        onChange={this.onRewardChange}
                        value={this.state.reward_id}
                      >
                        <option value="user_id">Select your option</option>
                        {this.state.rewards &&
                          this.state.rewards.length > 0 &&
                          this.state.rewards.map((item) => (
                            <option key={item.id} value={item.id}>
                              {item.reward_type
                                ? item.reward_type
                                : item.reward_type}
                            </option>
                          ))}
                      </CSelect>
                    </CFormGroup>
                    <button
                      className="btn btn-success btn-block"
                      disabled={this.state.loading || this.state.processing}
                    >
                      {(this.state.loading || this.state.processing) && (
                        <span className="spinner-border spinner-border-sm"></span>
                      )}
                      <span>SAVE AND UPDATE</span>
                    </button>
                    <br />
                    {this.state.message && !this.state.messageType && (
                      <div className="form-group">
                        <div className="alert alert-danger" role="alert">
                          {this.state.message}
                        </div>
                      </div>
                    )}
                    {this.state.message && this.state.messageType && (
                      <div className="form-group">
                        <div className="alert alert-info" role="alert">
                          {this.state.message}
                        </div>
                      </div>
                    )}
                  </CForm>
                </CCardBody>
              </CCard>
            </CCol>
          </CRow>
        </CContainer>
      );
    }
  }
}
