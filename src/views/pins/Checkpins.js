import React, { Component, useState, useEffect } from 'react'

import { useHistory, useLocation } from 'react-router-dom'
import {
    CButton,
    CCardHeader,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader,
    CModalTitle,
    CCard,
    CCardBody,
    CCardFooter,
    CSelect,
    CCol,
    CLabel,
    CFormGroup,
    CContainer,
    CForm,
    CInput,
    CInputGroup,
    CInputGroupPrepend,
    CInputGroupText,
    CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import Form from "react-validation/build/form";

import RestService from '../../_services/rest.service';
import Moment from 'moment';

export default class Checkpins extends Component {
  constructor(props) {
    super(props);
    this.onPinChange = this.onPinChange.bind(this);
    this.checkPin = this.checkPin.bind(this);
    this.onStatusChange = this.onStatusChange.bind(this);
    this.updatePin = this.updatePin.bind(this);
    // this.onBatchChange = this.onBatchChange.bind(this);
    // this.handleSubmit = this.handleSubmit.bind(this);
    // this.resetForm = this.resetForm.bind(this);
    // const history = useHistory()

    this.state = {
      modal:false,
      modal2:false,
      selectedUserId: '?',
      selectedStatus: '?',
      selectedBatchId: '?',
      start: '?',
      end: '?',
      users: [],
      pin_check:undefined,
      passedData: undefined,
      currentPage: "",
      products: [],
      productBatches: [],
      processing: false,
      loading: false,
      message: "",
      messageType: "",
      pin_value: ""
    };
  }

  componentDidMount() {
    this.getUsers()
  }

  setModal(){
      this.setState({
        modal: !this.state.modal,
      });
  }

  setModal2(){
      this.setState({
        modal2: !this.state.modal2,
      });
  }


  getUsers() {

  this.setState({
      message: "",
      loading: true
  });

  // this.form.validateAll();

      RestService.getAllUsers().then(
      (res) => {
          // console.log(res.data)


          this.setState({
              users: res.data,
              loading: false
          });
          // this.state = {users: res.data};

          // console.log(this.state.users)
          // this.props.history.push("/profile");
          // window.location.reload();
      },
      error => {
          const resMessage =
          (error.response &&
              error.response.data &&
              error.response.data.message) ||
          error.message ||
          error.toString();

          this.setState({
          loading: false,
          message: resMessage
          });
      }
      );
  }


  onPinChange(e) {
    this.setState({
      pin_value: e.target.value
    });
  }


  getUserProducts(event) {
      this.setState({
          selectedUserId: event.target.value
      });

      console.log(event.target.value)
      let id = event.target.value;
      this.setState({
      message: "",
      loading: true
      });
      RestService.getUserProducts(id).then(
      (res) => {
          // console.log(res.data)
          this.setState({
              products: res.data.products,
              loading: false
          });

          console.log(this.state.products)
      },
      error => {
          const resMessage =
          (error.response &&
              error.response.data &&
              error.response.data.message) ||
          error.message ||
          error.toString();

          this.setState({
          loading: false,
          message: resMessage
          });
      }
      );
  }


  getProductBatches(event) {
      console.log(event.target.value)
      let id = event.target.value;
      this.setState({
      message: "",
      selectedProdId: event.target.value,
      loading: true
      });

      RestService.getProductBatches(id).then(
      (res) => {
          this.setState({
              productBatches: res.data.productBatches,
              loading: false
          });

          console.log(this.state.productBatches)
      },
      error => {
          const resMessage =
          (error.response &&
              error.response.data &&
              error.response.data.message) ||
          error.message ||
          error.toString();

          this.setState({
          loading: false,
          message: resMessage
          });
      }
      );
  }

  onBatchChange(e) {
      this.setState({
          selectedBatchId: e.target.value
      });
      console.log(e.target.value)
  }

  onStatusChange(e) {
      this.setState({
        selectedStatus: e.target.value
      });
  }

  onEndChange(e) {
      this.setState({
          end: e.target.value
      });
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({
      message: "",
      processing: true
    });

    var data = {
          selectedUserId: this.state.selectedUserId,
          selectedProdId: this.state.selectedProdId,
          selectedBatchId: this.state.selectedBatchId,
          start: this.state.start,
          end: this.state.end,
      }

    console.log(data)
    // this.form.validateAll();

    RestService.reassignPins(data).then(
        () => {
          this.resetForm()
          this.setState({
            message: "Update successful",
            processing: false
          });
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            loading: false,
            message: resMessage
          });
        }
      );
  }


  checkPin(e) {
    e.preventDefault();

    this.setState({
      message: "",
      loading: true
    });

    var data = {
        pin_value: this.state.pin_value,
    }

    console.log(data)
    // this.form.validateAll();
    RestService.checkPin(data).then(
        (res) => {
          // this.resetForm()
          this.setState({
            pin_check: res.data,
            loading: false
          });
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            loading: false,
            message: resMessage
          });
        }
      );
  }

  updatePin(e) {
    e.preventDefault();

    this.setState({
      message: "",
      loading: true
    });

    var data = {
      pin_value: this.state.pin_value,
      status: this.state.selectedStatus,
    }

    console.log(data)
    // return;
    // this.form.validateAll();
    RestService.updatePin(data).then(
        (res) => {
          // this.resetForm()
          this.setState({
            message: "Succesfully updated",
            messageType:'success',
            loading: false
          });
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            loading: false,
            message: resMessage
          });
        }
      );
  }

  resetForm(){
      this.setState({
          selectedUserId: '',
          selectedProdId: '',
          selectedBatchId: '',
          start: '',
          end: '',
      });
  }
  render() {
    var _this = this;
      // console.log(this.state.users)
    //   <CDropdownItem onClick={() => _this.logout()}>

      return (
        <div className="c-app c-default-layout">
        <CModal
            show={_this.state.modal}
            // onClose={setModal}
            >
            <CModalHeader closeButton>
                <CModalTitle>Check Pin</CModalTitle>
            </CModalHeader>
            <CModalBody>
                  <Form
                    onSubmit={this.checkPin}
                  >
                    <CInputGroup className="mb-3">
                      <CInput type="text" value={this.state.pin_value} onChange={this.onPinChange} placeholder="Pin Value" />
                    </CInputGroup>
                    <CRow>
                      <CCol xs="12">
                        <button
                          className="btn btn-success btn-block"
                          disabled={this.state.loading}
                        >
                          {this.state.loading && (
                            <span className="spinner-border spinner-border-sm"></span>
                          )}
                          <span>Check</span>
                        </button>
                      </CCol>
                    </CRow>
                      {this.state.message && (
                        <div className="form-group">
                          <div className="alert alert-danger" role="alert">
                            {this.state.message}
                          </div>
                        </div>
                      )}
                  </Form>
                  <hr/>
                  <br/>
                    <div>
                      {this.state.pin_check && (
                        <div>
                          <p className="h4">{this.state.pin_check?this.state.pin_check.product.product_name:''}</p> <br/>
                          <p><b>Production Date:</b> {this.state.pin_check?this.state.pin_check.product.production_date:''}</p>
                          <p><b>Expiry Date:</b> {this.state.pin_check?this.state.pin_check.product.expiry_date:''}</p>
                          <p><b>Pin Status:</b> {this.state.pin_check?this.state.pin_check.pin_check.pin_status:''}</p>
                          <p><b>Producer Name:</b> {this.state.pin_check?this.state.pin_check.product.product.producer_name:''}</p>
                        </div>
                      )}
                    </div>
            </CModalBody>
            <CModalFooter>
                <CButton
                color="secondary"
                onClick={() => _this.setModal()}
                >Close</CButton>
            </CModalFooter>
        </CModal>
        <CModal
            show={_this.state.modal2}
            // onClose={setModal}
            >
            <CModalHeader closeButton>
                <CModalTitle>Update Pin</CModalTitle>
            </CModalHeader>
            <CModalBody>
                  <Form
                    onSubmit={this.updatePin}
                  >
                    <CInputGroup className="mb-3">
                      <CInput type="text" value={this.state.pin_value} onChange={this.onPinChange} placeholder="Pin Value" />
                    </CInputGroup>
                    <CCol xs="5">
                        <CFormGroup>
                            <CLabel htmlFor="ccproduct">Select Status</CLabel>
                            <CSelect custom name="ccproduct" id="ccproduct" onChange={this.onStatusChange}
                                value={this.state.selectedStatus}>
                                    <option value="" selected>Select your option</option>
                                    <option value='0'>0</option>
                                    <option value='1'>1</option>
                                    <option value='2'>2</option>
                            </CSelect>
                        </CFormGroup>
                    </CCol>
                    <CRow>
                      <CCol xs="12">
                        <button
                          className="btn btn-success btn-block"
                          disabled={this.state.loading}
                        >
                          {this.state.loading && (
                            <span className="spinner-border spinner-border-sm"></span>
                          )}
                          <span>Check</span>
                        </button>
                      </CCol>
                    </CRow>
                      {(this.state.message && !this.state.messageType )&& (
                        <div className="form-group">
                          <div className="alert alert-danger" role="alert">
                            {this.state.message}
                          </div>
                        </div>
                      )}
                      {(this.state.message && this.state.messageType )&& (
                        <div className="form-group">
                          <div className="alert alert-info" role="alert">
                            {this.state.message}
                          </div>
                        </div>
                      )}
                  </Form>
            </CModalBody>
            <CModalFooter>
                <CButton
                color="secondary"
                onClick={() => _this.setModal2()}
                >Close</CButton>
            </CModalFooter>
        </CModal>
          <CContainer>
            <CRow className="justify-content-center">
              <CCol md="10" lg="10" xl="10">
                <CCard className="mx-4">
                <CCardHeader>
                Get Pin Details and Status
              </CCardHeader>
                  <CCardBody className="d-flex">
                    <CButton color="primary" onClick={() => _this.setModal()} className="mr-1 mt-2 mb-2 flex-fill">
                      Check Pin Validity
                    </CButton>
                    <CButton color="primary" onClick={() => _this.setModal2()} className="mr-2 mt-2 mb-2 flex-fill">
                      Update Pin Status
                    </CButton>
                  </CCardBody>
                </CCard>
              </CCol>
            </CRow>
          </CContainer>
        </div>
      )
  }

}
