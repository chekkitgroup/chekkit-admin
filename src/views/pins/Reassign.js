import React, { Component, useState, useEffect } from 'react'

import { useHistory, useLocation } from 'react-router-dom'
import {
    CButton,
    CCard,
    CCardBody,
    CCardFooter,
    CSelect,
    CCol,
    CLabel,
    CFormGroup,
    CContainer,
    CForm,
    CInput,
    CInputGroup,
    CInputGroupPrepend,
    CInputGroupText,
    CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

import RestService from '../../_services/rest.service';
import Moment from 'moment';

export default class Users extends Component {
  constructor(props) {
    super(props);
    this.getUserProducts = this.getUserProducts.bind(this);
    this.getProductBatches = this.getProductBatches.bind(this);
    this.onStartChange = this.onStartChange.bind(this);
    this.onEndChange = this.onEndChange.bind(this);
    this.onBatchChange = this.onBatchChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.resetForm = this.resetForm.bind(this);
    // const history = useHistory()

    this.state = {
      modal:false,
      selectedUserId: '',
      selectedProdId: '',
      selectedBatchId: '',
      start: '',
      end: '',
      users: [],
      passedData: undefined,
      currentPage: "",
      products: [],
      productBatches: [],
      processing: false,
      loading: false,
      message: "",
      messageType: "",
    };
  }

  componentDidMount() {
    this.getUsers()
    // this.useHistoryCall()
  }

    getUsers() {

    this.setState({
        message: "",
        loading: true
    });

    // this.form.validateAll();

        RestService.getAllUsers().then(
        (res) => {
            // console.log(res.data)


            this.setState({
                users: res.data,
                loading: false
            });
            // this.state = {users: res.data};

            // console.log(this.state.users)
            // this.props.history.push("/profile");
            // window.location.reload();
        },
        error => {
            const resMessage =
            (error.response &&
                error.response.data &&
                error.response.data.message) ||
            error.message ||
            error.toString();

            this.setState({
            loading: false,
            message: resMessage
            });
        }
        );
    }



    getUserProducts(event) {
        this.setState({
            selectedUserId: event.target.value
        });

        console.log(event.target.value)
        let id = event.target.value;
        this.setState({
        message: "",
        loading: true
        });
        RestService.getUserProducts(id).then(
        (res) => {
            // console.log(res.data)


            this.setState({
                products: res.data.products,
                loading: false
            });

            console.log(this.state.products)
        },
        error => {
            const resMessage =
            (error.response &&
                error.response.data &&
                error.response.data.message) ||
            error.message ||
            error.toString();

            this.setState({
            loading: false,
            message: resMessage
            });
        }
        );
    }


    getProductBatches(event) {
        console.log(event.target.value)
        let id = event.target.value;
        this.setState({
        message: "",
        selectedProdId: event.target.value,
        loading: true
        });

        RestService.getProductBatches(id).then(
        (res) => {
            this.setState({
                productBatches: res.data.productBatches,
                loading: false
            });

            console.log(this.state.productBatches)
        },
        error => {
            const resMessage =
            (error.response &&
                error.response.data &&
                error.response.data.message) ||
            error.message ||
            error.toString();

            this.setState({
            loading: false,
            message: resMessage
            });
        }
        );
    }



    onBatchChange(e) {
        this.setState({
            selectedBatchId: e.target.value
        });
        console.log(e.target.value)
    }

    onStartChange(e) {
        this.setState({
            start: e.target.value
        });
    }

    onEndChange(e) {
        this.setState({
            end: e.target.value
        });
    }

    handleSubmit(e) {
      e.preventDefault();

      this.setState({
        message: "",
        processing: true
      });

      var data = {
            selectedUserId: this.state.selectedUserId,
            selectedProdId: this.state.selectedProdId,
            selectedBatchId: this.state.selectedBatchId,
            start: this.state.start,
            end: this.state.end,
        }

      console.log(data)
      // this.form.validateAll();

      RestService.reassignPins(data).then(
          () => {
            this.resetForm()
            this.setState({
              message: "Update successful",
              processing: false
            });
          },
          error => {
            const resMessage =
              (error.response &&
                error.response.data &&
                error.response.data.message) ||
              error.message ||
              error.toString();

            this.setState({
              loading: false,
              message: resMessage,
              processing: false
            });
          }
        );
    }
resetForm(){
    this.setState({
        selectedUserId: '',
        selectedProdId: '',
        selectedBatchId: '',
        start: '',
        end: '',
    });
}
  render() {
    var _this = this;
    if(this.state.users) {
      // console.log(this.state.users)
    //   <CDropdownItem onClick={() => _this.logout()}>

      return (
        <div className="c-app c-default-layout">
          <CContainer>
            <CRow className="justify-content-center">
              <CCol md="10" lg="10" xl="10">
                <CCard className="mx-4">
                  <CCardBody className="p-4">
                    <CForm
                        onSubmit={this.handleSubmit}
                        // ref={c => {
                        //     this.form = c;
                        // }}
                        >
                      <p className="text-muted">Reassign Pins</p>
                        <CRow>
                            <CCol xs="4">
                                <CFormGroup>
                                    <CLabel htmlFor="ccaccount">Select Account</CLabel>
                                    <CSelect custom name="ccaccount" id="ccaccount" onChange={this.getUserProducts}
                                    value={this.state.selectedUserId}
                                    >
                                    <option value="">Select your option</option>
                                    {this.state.users.map(item => (
                                        <option
                                            key={item.id}
                                            value={item.id}>
                                            {item.company_rep?item.company_rep+ " " + '('+ ' ' + item.username + ' ' +')':'' + ' - ' + item.first_name?item.first_name:''}
                                        </option>
                                    ))}
                                    </CSelect>
                                </CFormGroup>
                            </CCol>
                            <CCol xs="4">
                                <CFormGroup>
                                    <CLabel htmlFor="ccproduct">Select Product</CLabel>
                                    <CSelect custom name="ccproduct" id="ccproduct" onChange={this.getProductBatches}
                                        value={this.state.selectedProdId}>
                                        <option value="">Select your option</option>
                                        {this.state.products.map(item => (
                                            <option
                                                key={item.id}
                                                value={item.id}>
                                                {item.product_name?item.product_name:''}
                                            </option>
                                        ))}
                                    </CSelect>
                                </CFormGroup>
                            </CCol>
                            <CCol xs="4">
                                <CFormGroup>
                                    <CLabel htmlFor="ccbatch">Select Batch to assign pin</CLabel>
                                    <CSelect custom name="ccbatch" id="ccbatch" value={this.state.selectedBatchId}  onChange={this.onBatchChange}>
                                        <option value="">Select your option</option>
                                        {this.state.productBatches.map(item => (
                                            <option
                                                key={item.id}
                                                value={item.id}>
                                                {item.product_name?item.product_name:'' + ' - ' + item.batch_num?item.batch_num:''}
                                            </option>
                                        ))}
                                    </CSelect>
                                </CFormGroup>
                            </CCol>
                        </CRow>

                        <CRow>
                            <CCol xs="6">
                                <CFormGroup>
                                    <CLabel htmlFor="ccbatch">Start Index</CLabel>
                                    <CInput type="text" value={this.state.start} onChange={this.onStartChange} placeholder="start" />
                                </CFormGroup>
                            </CCol>
                            <CCol xs="6">
                                <CFormGroup>
                                    <CLabel htmlFor="ccbatch">End Index</CLabel>
                                    <CInput type="text" value={this.state.end} onChange={this.onEndChange} placeholder="end" />
                                </CFormGroup>
                            </CCol>
                        </CRow>
                          <button
                            className="btn btn-success btn-block"
                            disabled={(this.state.loading || this.state.processing)}
                          >
                            {(this.state.loading || this.state.processing)&& (
                              <span className="spinner-border spinner-border-sm"></span>
                            )}
                            <span>Submit</span>
                          </button>
                          <br/>
                      {(this.state.message && !this.state.messageType )&& (
                        <div className="form-group">
                          <div className="alert alert-danger" role="alert">
                            {this.state.message}
                          </div>
                        </div>
                      )}
                      {(this.state.message && this.state.messageType )&& (
                        <div className="form-group">
                          <div className="alert alert-info" role="alert">
                            {this.state.message}
                          </div>
                        </div>
                      )}
                    </CForm>
                  </CCardBody>
                </CCard>
              </CCol>
            </CRow>
          </CContainer>
        </div>
      )
    }
  }

}
