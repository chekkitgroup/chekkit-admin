import React, { useState, useEffect } from "react";

import {
  CButton,
  CCardHeader,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CCard,
  CCardBody,
  CCardFooter,
  CSelect,
  CCol,
  CLabel,
  CFormGroup,
  CContainer,
  CInput,
  CInputGroup,
  CRow,
  CPagination,
} from "@coreui/react";

import Form from "react-validation/build/form";

import RestService from "../../_services/rest.service";

const PinCheck = () => {
  const [currentPage, setActivePage] = useState(1);
  const [numsPage, setNumsPage] = useState(1);
  const [state, setState] = useState({
    modal: false,
    modal2: false,
    sequence_check: undefined,
    serial_check: undefined,
    passedData: undefined,
    currentPage: "",
    loading: false,
    message: "",
    messageType: "",
    sequence_value: "",
    serial_value: "",
  });

  const setModal = () => {
    setState({
      ...state,
      sequence_value: "",
      sequence_check: "",
      message: "",
      modal: !state.modal,
    });
    setNumsPage(1);
    setActivePage(1);
  };

  const setModal2 = () => {
    setState({
      ...state,
      serial_value: "",
      serial_check: "",
      message: "",
      modal2: !state.modal2,
    });
  };

  const onSequenceChange = (e) => {
    setState({ ...state, sequence_value: e.target.value });
  };

  const onSerialChange = (e) => {
    setState({ ...state, serial_value: e.target.value });
  };

  const getNumsPages = (comment) => {
    return Math.ceil(parseFloat(comment.split(" ")[3]) / 15);
  };

  useEffect(() => {
    if (state.sequence_value) {
      RestService.getPinsBySequence(
        state,
        `page=${currentPage}&per_page=15`
      ).then(
        (res) => {
          // this.resetForm()
          setState({
            ...state,
            sequence_check: res.data,
            loading: false,
            message: "",
          });
          setNumsPage(getNumsPages(res.data.metadata.comment));

          if (!res.status) {
            setState({ ...state, loading: false, message: res.message });
          }
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          setState({ ...state, loading: false, message: resMessage });
        }
      );
    }
  }, [currentPage]);

  const checkSequence = (e) => {
    e.preventDefault();

    setState({ ...state, message: "", loading: true });

    var data = {
      sequence_value: state.sequence_value,
    };

    console.log(data);
    // this.form.validateAll();
    RestService.getPinsBySequence(data).then(
      (res) => {
        // this.resetForm()
        setState({
          ...state,
          sequence_check: res.data,
          loading: false,
          message: "",
        });

        console.log("Sequence Data", res.data);

        setNumsPage(getNumsPages(res.data.metadata.comment));

        if (!res.status) {
          setState({ ...state, loading: false, message: res.message });
        }
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setState({ ...state, loading: false, message: resMessage });
      }
    );
  };

  const checkSerial = (e) => {
    e.preventDefault();

    setState({ ...state, message: "", loading: true });

    var data = {
      serial_value: state.serial_value,
    };

    // this.form.validateAll();
    RestService.getPinBySerialNumber(data).then(
      (res) => {
        // this.resetForm()
        setState({
          ...state,
          serial_check: res.data,
          loading: false,
          message: "",
        });
        console.log("Serial Data", res.data);

        if (!res.status) {
          setState({ ...state, loading: false, message: res.message });
        }
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setState({ ...state, loading: false, message: resMessage });
      }
    );
  };

  return (
    <div className="c-app c-default-layout">
      <CModal
        show={state.modal}
        // onClose={setModal}
      >
        <CModalHeader closeButton>
          <CModalTitle>Check Pin Sequence</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <Form onSubmit={checkSequence}>
            <CInputGroup className="mb-3">
              <CInput
                type="text"
                value={state.sequence_value}
                onChange={onSequenceChange}
                placeholder="Pin Sequence"
                required
              />
            </CInputGroup>
            <CRow>
              <CCol xs="12">
                <button
                  className="btn btn-success btn-block"
                  disabled={state.loading}
                >
                  {state.loading && (
                    <span className="spinner-border spinner-border-sm"></span>
                  )}
                  <span>Check</span>
                </button>
              </CCol>
            </CRow>
            {state.message && (
              <div className="form-group">
                <div className="alert alert-danger" role="alert">
                  {state.message}
                </div>
              </div>
            )}
          </Form>
          <hr />
          <br />
          {state.sequence_check && (
            <ul role="list" style={{ listStyle: "square" }}>
              {state.sequence_check.pins.map((data, idx) => {
                return (
                  <li key={idx} className="p-4">
                    <div>{`Product Name: ${data?.SubProduct?.product_name ? data?.SubProduct?.product_name : `Unassigned`}`}</div>
                    <div>{`Serial Number: ${data?.id}`}</div>
                    <div>{`Pin: ${data?.pin_value}`}</div>
                  </li>
                );
              })}
            </ul>
          )}
        </CModalBody>
        <CModalFooter className="flex justify-content-between">
          <div className={`mt-3`}>
            {state.sequence_check && (
              <CPagination
                activePage={currentPage}
                pages={numsPage}
                onActivePageChange={(i) => setActivePage(i)}
              ></CPagination>
            )}
          </div>
          <CButton color="secondary" onClick={() => setModal()}>
            Close
          </CButton>
        </CModalFooter>
      </CModal>

      <CModal
        show={state.modal2}
        // onClose={setModal}
      >
        <CModalHeader closeButton>
          <CModalTitle>Check Serial Number</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <Form onSubmit={checkSerial}>
            <CInputGroup className="mb-3">
              <CInput
                type="text"
                value={state.serial_value}
                onChange={onSerialChange}
                placeholder="Serial Number"
                required
              />
            </CInputGroup>
            <CRow>
              <CCol xs="12">
                <button
                  className="btn btn-success btn-block"
                  disabled={state.loading}
                >
                  {state.loading && (
                    <span className="spinner-border spinner-border-sm"></span>
                  )}
                  <span>Check</span>
                </button>
              </CCol>
            </CRow>
            {state.message && (
              <div className="form-group">
                <div className="alert alert-danger" role="alert">
                  {state.message}
                </div>
              </div>
            )}
          </Form>
          <hr />
          <br />
          {state.serial_check && (
            <ul role="list" style={{ listStyle: "square" }}>
              <li className="p-4">
                <div>{`Product Name: ${state.serial_check?.SubProduct?.product_name ? state.serial_check?.SubProduct?.product_name : `Unassigned`}`}</div>
                <div>{`Serial Number: ${state.serial_check?.id}`}</div>
                <div>{`Pin: ${state.serial_check?.pin_value}`}</div>
              </li>
            </ul>
          )}
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setModal2()}>
            Close
          </CButton>
        </CModalFooter>
      </CModal>

      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="10" lg="10" xl="10">
            <CCard className="mx-4">
              <CCardHeader>
                Check Pin by Serial Number or by Part of Pin
              </CCardHeader>
              <CCardBody className="d-flex">
                <CButton
                  color="primary"
                  onClick={() => setModal2()}
                  className="mr-1 mt-2 mb-2 flex-fill"
                >
                  Serial Number
                </CButton>
                <CButton
                  color="primary"
                  onClick={() => setModal()}
                  className="mr-2 mt-2 mb-2 flex-fill"
                >
                  Pin Sequence
                </CButton>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default PinCheck;
