import React, { lazy, useEffect } from "react";
import { useHistory } from "react-router-dom";
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CCallout,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";

import MainChartExample from "../charts/MainChartExample.js";

const WidgetsDropdown = lazy(() => import("../widgets/WidgetsDropdown.js"));
const WidgetsBrand = lazy(() => import("../widgets/WidgetsBrand.js"));

const Dashboard = () => {
  const history = useHistory()

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem('chekkitSu'))?.user
    if (user.membership_type !== 'admin') {
      history.push('product-pins/pin-check')
    }
  }, [])

  return (
    <>
      <WidgetsDropdown />
    </>
  );
};

export default Dashboard;
