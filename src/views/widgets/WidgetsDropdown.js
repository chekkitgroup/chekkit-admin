import React, { Component } from "react";
import {
  CWidgetDropdown,
  CRow,
  CCol,
  CDropdown,
  CDropdownMenu,
  CDropdownItem,
  CDropdownToggle,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import RestService from "../../_services/rest.service";

// const WidgetsDropdown = () => {
export default class WidgetsDropdown extends Component {
  constructor(props) {
    super(props);

    this.state = {
      stats: {},
      username: "",
      currentPage: "",
      password: "",
      loading: false,
      message: "",
    };
  }

  componentDidMount() {
    this.getStats();
    // this.setPage()
  }

  getStats() {
    this.setState({
      loading: true,
      message: "",
    });

    RestService.getDashboardStats().then(
      (res) => {
        console.log(res);
        this.setState({
          stats: res.data.stats,
        });
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        this.setState({
          loading: false,
          message: resMessage,
        });
      }
    );
  }

  render() {
    // render
    return (
      <CRow>
        <CCol sm="6" lg="3">
          <CWidgetDropdown
            color="gradient-primary"
            header={JSON.stringify(this.state.stats.users)}
            text="Total Signups"
            footerSlot={<br />}
          >
            <CDropdown>
              <CDropdownToggle color="transparent">
                <CIcon name="cil-settings" />
              </CDropdownToggle>
              <CDropdownMenu className="pt-0" placement="bottom-end">
                <CDropdownItem>View</CDropdownItem>
                <CDropdownItem>Add</CDropdownItem>
              </CDropdownMenu>
            </CDropdown>
          </CWidgetDropdown>
        </CCol>

        <CCol sm="6" lg="3">
          <CWidgetDropdown
            color="gradient-info"
            header={JSON.stringify(this.state.stats.pins)}
            text="Generated Pins"
            footerSlot={<br />}
          >
            <CDropdown>
              <CDropdownToggle caret={false} color="transparent">
                <CIcon name="cil-location-pin" />
              </CDropdownToggle>
              <CDropdownMenu className="pt-0" placement="bottom-end">
                <CDropdownItem>View</CDropdownItem>
                <CDropdownItem>Add</CDropdownItem>
              </CDropdownMenu>
            </CDropdown>
          </CWidgetDropdown>
        </CCol>

        <CCol sm="6" lg="3">
          <CWidgetDropdown
            color="gradient-warning"
            header={JSON.stringify(this.state.stats.scans)}
            text="Authenticated Pins"
            footerSlot={<br />}
          >
            <CDropdown>
              <CDropdownToggle color="transparent">
                <CIcon name="cil-settings" />
              </CDropdownToggle>
              <CDropdownMenu className="pt-0" placement="bottom-end">
                <CDropdownItem>View</CDropdownItem>
                <CDropdownItem>Add</CDropdownItem>
              </CDropdownMenu>
            </CDropdown>
          </CWidgetDropdown>
        </CCol>

        <CCol sm="6" lg="3">
          <CWidgetDropdown
            color="gradient-danger"
            header={JSON.stringify(this.state.stats.products)}
            text="Total Products"
            footerSlot={<br />}
          >
            <CDropdown>
              <CDropdownToggle caret className="text-white" color="transparent">
                <CIcon name="cil-settings" />
              </CDropdownToggle>
              <CDropdownMenu className="pt-0" placement="bottom-end">
                <CDropdownItem>View</CDropdownItem>
                <CDropdownItem>Add</CDropdownItem>
              </CDropdownMenu>
            </CDropdown>
          </CWidgetDropdown>
        </CCol>
      </CRow>
    );
  }
}
