import React, { Component, useState } from "react";
import {
  CBadge,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { logoutUser } from "../actions/user";
import AuthService from "../_services/authentication.service";
import { Route, withRouter, useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import { Redirect } from "react-router";

const TheHeaderDropdown = () => {
  const history = useHistory();
  const [state, setState] = useState({
    username: "",
    redirect: false,
  });

  const logout = () => {
    AuthService.logout();

    setState({
      ...state,
      redirect: true,
    });
  };

  return (
    <CDropdown inNav className="c-header-nav-items mx-2" direction="down">
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <div className="c-avatar">
          <CImg
            src={"avatars/6.jpg"}
            className="c-avatar-img"
            alt="admin@bootstrapmaster.com"
          />
        </div>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownItem header tag="div" color="light" className="text-center">
          <strong>Settings</strong>
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-user" className="mfe-2" />
          Profile
        </CDropdownItem>

        <CDropdownItem onClick={() => history.push("/settings")}>
          <CIcon name="cil-settings" className="mfe-2" />
          Settings
        </CDropdownItem>

        <CDropdownItem>
          <CIcon name="cil-credit-card" className="mfe-2" />
          Payments
        </CDropdownItem>
        <CDropdownItem divider />
        <CDropdownItem onClick={() => logout()}>
          <CIcon name="cil-lock-locked" className="mfe-2" />
          Log out
        </CDropdownItem>
      </CDropdownMenu>
      {state.redirect && <Redirect to="/login" />}
    </CDropdown>
  );
};

TheHeaderDropdown.contextTypes = {
  router: PropTypes.object,
};

export default TheHeaderDropdown;
