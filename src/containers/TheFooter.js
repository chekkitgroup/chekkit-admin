import React from 'react'
import { CFooter } from '@coreui/react'

const TheFooter = () => {
  return (
    <CFooter fixed={false}>
      <div className="mfs-auto">
        <a href="https://chekkit.com" target="_blank" rel="noopener noreferrer">Chekkit Admin Portal</a>
      </div>
    </CFooter>
  )
}

export default React.memo(TheFooter)
